package com.hoodmc.attack.obj;

import com.hoodmc.attack.enums.GUIViewAction;
import com.hoodmc.attack.enums.GUIViewType;
import com.hoodmc.attack.enums.Turret;
import com.hoodmc.attack.utils.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by deadlyscone on 11/7/2015.
 */
public class ActionBlockObject {

    private ItemStack itemStack;
    private GUIViewAction action; private GUIViewType view;// Only has one or the either.
    private Integer index;


    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Enum getAction() {
        if (this.view != null){
            return this.view;
        }else if (this.view != null){
            return this.action;
        }
        return null;
    }

    //******************************************************************\
    //  Sets the ViewType or ViewAction depending if there is an action
    //  tag specified with the ViewAction or will set the ViewType
    //  if a ViewType is specified which will ignore any action tags.
    //******************************************************************\
    public void setAction(Enum guiEnumeration) {
        if(guiEnumeration instanceof GUIViewAction){
            this.action = ((GUIViewAction)guiEnumeration);
            String name = this.getItemStack().getItemMeta().getDisplayName();

            Pattern pattern = Pattern.compile("\\[(.*?)\\]");
            Matcher matcher = pattern.matcher(name);
            if (matcher.find()) {
                String guiTag = matcher.group(1);
                ItemMeta im = this.itemStack.getItemMeta();
                im.setDisplayName(name.replace("[" + guiTag + "]", Turret.Type.AsFString(guiTag)) +
                        HelperUtil.encodeItemData(DataManager.SCHAR + guiTag));


                if(this.action == GUIViewAction.BUY_TURRET){
                    TurretObject parentObj = TurretManager.getParentObject(Turret.Type.asTurretType(guiTag, true));
                    if(parentObj == null){
                        PluginLogger.warning("You have an incompatible configuration tag ["+guiTag+"] associated with the " +
                                "GUIViewAction. Please check your configuration.");
                    }else{
                        List<String> lores = new ArrayList<String>();
                        lores.add("Cost: " + parentObj.getPurchaseCost());
                        lores.add("HP: " + parentObj.getHealth());
                        lores.add("Hit Damage: " + parentObj.getDamage());
                        lores.add("Repair Time: " + parentObj.getRepairTime());
                        im.setLore(lores);
                    }
                }else if(this.action == GUIViewAction.BUY_ATTACKER){
                    //ATTACKER LORE INFO HERE
                }
                this.getItemStack().setItemMeta(im);


            }
        }else if(guiEnumeration instanceof GUIViewType){
            this.view = ((GUIViewType)guiEnumeration);
        }
    }
}
