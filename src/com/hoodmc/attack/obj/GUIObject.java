package com.hoodmc.attack.obj;

import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.enums.GUIViewType;
import com.hoodmc.attack.enums.InventorySize;
import com.hoodmc.attack.utils.DataManager;
import com.hoodmc.attack.utils.GUIManager;
import com.hoodmc.attack.utils.HelperUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deadlyscone on 11/3/2015.
 */
public class GUIObject /*implements Cloneable*/{

    private String name;
    private ItemStack fillItem;
    private InventorySize size;
    private GUIViewType type;
    private List<ActionBlockObject> actionBlocks;

    //******************************************************************\
    //  type get; set;
    //******************************************************************\
    public GUIViewType getType() {
        return type;
    }

    public void setType(GUIViewType type) {
        this.type = type;
    }


    //******************************************************************\
    //  name get; set;
    //******************************************************************\
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = ChatColor.translateAlternateColorCodes('&', name);
    }


    //******************************************************************\
    //  fillItem get; set;
    //******************************************************************\
    public ItemStack getFillItem() {
        return fillItem;
    }

    public void setFillItem(String item) {
        this.fillItem = asItemStack(item);
    }

    //******************************************************************\
    //  size get; set;
    //******************************************************************\
    public InventorySize getSize() {
        return size;
    }

    public void setSize(InventorySize size) {
        this.size = size;
    }

    //******************************************************************\
    //  actionBlocks get; set;
    //******************************************************************\
    public ActionBlockObject getActionBlock(Integer index) {
        for(ActionBlockObject action : this.actionBlocks){
            if(action.getIndex() == index){ return action; }
        }
        return null;
    }
    public ActionBlockObject getActionBlock(Enum obj) {
        for(ActionBlockObject blk : this.actionBlocks){
            if(blk.getAction() == obj){ return blk; }
        }
        return null;
    }
    public List<ActionBlockObject> getActionBlocks() {
        return this.actionBlocks;
    }

    public void setActionBlocks(List<String> itemData) {
        try {
            List<ActionBlockObject> actionBlocks = new ArrayList<ActionBlockObject>();
            for(String s : itemData){
                String[] data = s.split(",");
                ActionBlockObject actBlk = new ActionBlockObject();
                String name = ChatColor.translateAlternateColorCodes('&', data[4]);
                Integer id = Integer.valueOf(data[0]);
                Short dmg = Short.valueOf(data[1]);
                Integer index = Integer.valueOf(data[2])-1;
                Enum action = GUIManager.translateEnumFromString(data[3], true);
                ItemStack item = new ItemStack(id,1,dmg);

                ItemMeta im = item.getItemMeta();
                im.setDisplayName(name + HelperUtil.encodeItemData(action.toString()));
                im.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                item.setItemMeta(im);
                actBlk.setItemStack(item);
                actBlk.setAction(action);
                actBlk.setIndex(index);
                actionBlocks.add(actBlk);
            }
            this.actionBlocks = actionBlocks;
        }catch(Exception ex){
            AttackPlugin.halt(ex);
        }
    }

    //******************************************************************\
    //  HelperUtil Functions
    //******************************************************************\

    private ItemStack asItemStack(String itemData){
        try {
            String[] data = itemData.split(",");

            String name = ChatColor.translateAlternateColorCodes('&', data[2]);
            Integer id = Integer.valueOf(data[0]);
            Short dmg = Short.valueOf(data[1]);
            ItemStack item = new ItemStack(id,1,dmg);
            if(item.getType() != Material.AIR){
                ItemMeta im = item.getItemMeta();
                im.setDisplayName(name);
                item.setItemMeta(im);
            }
            return item;
        }catch(Exception ex){
            AttackPlugin.halt(ex);
            return null;
        }
    }

    public void displayGUI(Player player){
        Inventory gui = Bukkit.getServer().createInventory(null, InventorySize.asInteger(this.size), this.name);

        try {
            for(int i = 0; i < InventorySize.asInteger(this.size); i++){
                if(this.getActionBlock(i) != null){
                    gui.setItem(i, this.getActionBlock(i).getItemStack());
                }else{
                    gui.setItem(i, this.fillItem);
                }
            }
            //open the inventory
            DataManager.OpenGUIs.put(player.getUniqueId(), gui);
            player.openInventory(gui);

        }catch(Exception ex){
            ex.printStackTrace();
            //player.closeInventory();
            //player.sendMessage();
        }

    }

    /*public GUIObject clone() {
        try {
            GUIObject newOBJ = (GUIObject)super.clone();
            DataManager.GUIObjects.add(newOBJ);
            return newOBJ;
        }
        catch (CloneNotSupportedException e) {
            AttackPlugin.halt(e);
            return null;
        }
    }*/
}