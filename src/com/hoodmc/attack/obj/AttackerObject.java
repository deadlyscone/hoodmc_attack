package com.hoodmc.attack.obj;

import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.utils.HelperUtil;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.hoodmc.attack.enums.AttackerType;

import java.io.Serializable;
import java.util.UUID;

@SuppressWarnings("serial")
public class AttackerObject implements Cloneable, Serializable{

	private UUID attackerUUID;
	private UUID npcUUID;
	private AttackerType type;
	private Double purchaseCost;
	private ItemStack
			helmet,
			chestplate,
			leggings,
			boots,
			hand;
	private String name;
	private int health;
	private Double walkSpeed;
	private EntityType entityType;
	private int fireRate;
	private int range;
	private double damage;
	private String particle;
	private Entity targetEnt = null;
	private boolean exists = false;
	
	private int attackTimer = 0;

	public AttackerObject() {
		//Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){ public void run(){timer();}}, 5, (20));
	}


	//******************************************************************\
	//  mobType get; set;
	//******************************************************************\
	public boolean exists() {
		return exists;
	}

	public void setExists(boolean exists) {
		this.exists = exists;
	}

	//******************************************************************\
	//  mobType get; set;
	//******************************************************************\
	public EntityType getMobType() { return entityType; }

	public void setEntityType(String mobType) {
		try{ this.entityType = EntityType.valueOf(mobType.toUpperCase().replace(" ","_"));
		}catch (Exception ex){ AttackPlugin.halt(ex); }
	}

	//******************************************************************\
	//  npcUUID get; set;
	//******************************************************************\
	public UUID getNpcUUID() {
		return npcUUID;
	}

	public NPC getNPC(){
		NPCRegistry registry = CitizensAPI.getNPCRegistry();
		return registry.getByUniqueId(this.getNpcUUID());
	}

	public void setNpcUUID(UUID npcUUID) {
		this.npcUUID = npcUUID;
	}

	//******************************************************************\
	//  walkSpeed get; set;
	//******************************************************************\
	public Double getWalkSpeed() { return walkSpeed; }

	public void setWalkSpeed(Double walkSpeed) { this.walkSpeed = walkSpeed; }

	//******************************************************************\
	//  purchaseCost get; set;
	//******************************************************************\
	public double getPurchaseCost() { return purchaseCost; }

	public void setPurchaseCost(double purchaseCost) { this.purchaseCost = purchaseCost; }

	//******************************************************************\
	//  fireRate get; set;
	//******************************************************************\
	public Integer getFireRate() { return fireRate; }

	public void setFireRate(Integer fireRate) { this.fireRate = fireRate; }
	
	//******************************************************************\
	//  helmet get; set;
	//******************************************************************\
	public ItemStack getHelmet() {
		return helmet;
	}

	public void setHelmet(String helmet) {
		this.helmet = HelperUtil.toItemStack(helmet, ",");
	}
	
	//******************************************************************\
	//  particle get; set;
	//******************************************************************\
	public String getParticle() {
		return particle;
	}

	public void setParticle(String effect) {
		this.particle = effect;
	}

	//******************************************************************\
	//  chestplate get; set;
	//******************************************************************\
	public ItemStack getChestplate() {
		return chestplate;
	}

	public void setChestplate(String chestplate) {
		
		this.chestplate = HelperUtil.toItemStack(chestplate, ",");
	}

	//******************************************************************\
	//  pants get; set;
	//******************************************************************\
	public ItemStack getLeggings() {
		return leggings;
	}

	public void setLeggings(String leggings) {
		this.leggings = HelperUtil.toItemStack(leggings, ",");
	}

	//******************************************************************\
	//  boots get; set;
	//******************************************************************\
	public ItemStack getBoots() {
		return boots;
	}

	public void setBoots(String boots) {
		this.boots = HelperUtil.toItemStack(boots, ",");
	}

	//******************************************************************\
	//  hand get; set;
	//******************************************************************\
	public ItemStack getHand() {
		return hand;
	}

	public void setHand(String hand) {
		this.hand = HelperUtil.toItemStack(hand, ",");
	}

	//******************************************************************\
	//  name get; set;
	//******************************************************************\
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	//******************************************************************\
	//  attackerUUID get; set;
	//******************************************************************\
	public UUID getAttackerUUID() {
		return attackerUUID;
	}

	public void setAttackerUUID(UUID attackerUUID) {
		this.attackerUUID = attackerUUID;
	}

	//******************************************************************\
	//  health get; set;
	//******************************************************************\
	public int getHealth() {
		return health;
	}
	
	public void setHealth(int health) {
		this.health = health;
	}

	public void setHealth(String health) {
		this.health = Integer.parseInt(health);
	}
	
	//******************************************************************\
	//  range get; set;
	//******************************************************************\
	public int getRange() {
		return range;
	}

	public void setRange(int r) {
		range = r;
	}


	//******************************************************************\
	//  type get; set;
	//******************************************************************\
	public AttackerType getType() {
		return type;
	}

	public void setType(AttackerType type) {
		this.type = type;
	}

	//******************************************************************\
	//  targetEnt get; set;
	//******************************************************************\
	public void setTarget(Entity e){
		targetEnt = e;
		if(targetEnt instanceof EnderCrystal){
			this.getNPC().getNavigator().setTarget(targetEnt.getLocation());
		}else
			this.getNPC().getNavigator().setTarget(targetEnt, true);
		//System.out.println("Target");
	}
	
	public Entity getTarget(){
		return targetEnt;
	}

	public void clearTarget(){
		this.getNPC().getNavigator().setTarget(this.getNPC().getEntity().getLocation());
		targetEnt = null;
	}

	public void stopWalk(){
		this.getNPC().getNavigator().setTarget(this.getNPC().getEntity().getLocation());
	}
	
	//******************************************************************\
	//  damage get;
	//******************************************************************\
	public Double getDamage() {
		return damage;
	}

	public void setDamage(Double damage) {
		this.damage = damage;
	}
	
	//******************************************************************\
	//  Helper functions
	//******************************************************************\
	@SuppressWarnings({ "deprecation", "unused" })
	private ItemStack asItemStack(String itemData){
		String[] data = itemData.split(",");
		int id = Integer.valueOf(data[1]);
		int amount = Integer.valueOf(data[2]);
		Short dmg = Short.valueOf(data[3]);
		ItemStack item = new ItemStack(id, amount, dmg);
		ItemMeta im = item.getItemMeta();
		im.setDisplayName(ChatColor.translateAlternateColorCodes('&', data[0]));
		item.setItemMeta(im);
		
		return item;
	}

	public void setLookAt(Location l){
		this.getNPC().faceLocation(l);
	}

	public AttackerObject clone() {
		try {
			AttackerObject newOBJ = (AttackerObject)super.clone();
			newOBJ.setAttackerUUID(UUID.randomUUID());
			return newOBJ;
		}
		catch (CloneNotSupportedException e) {
			AttackPlugin.halt(e);
			return null;
		}
	}


	public int getAttackTimer() {
		return attackTimer;
	}


	public void setAttackTimer(int attackTimer) {
		this.attackTimer = attackTimer;
	}
	
	public void delete(){
		try{this.getNPC().despawn();}catch(Exception x){}
		try{this.getNPC().destroy();}catch(Exception x){}
		exists = false;
	}
	
	public void kill(AttackPlugin plugin){
		((LivingEntity)getNPC().getEntity()).setHealth(0);
	    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
	        public void run(){
	        	delete();
	        }
	      }, 25L);
	}
}