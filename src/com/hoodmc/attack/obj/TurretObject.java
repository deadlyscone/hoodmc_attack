package com.hoodmc.attack.obj;

import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.enums.Turret;
import com.hoodmc.attack.enums.Turret.Type;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.*;
import org.bukkit.util.*;
import org.bukkit.util.Vector;

import java.io.Serializable;
import java.util.*;

public class TurretObject implements Cloneable, Serializable{
	private static final long serialVersionUID = -825676998082391488L;
	/*
	PLEASE DO NOT MAKE CHANGES TO THE PARENT OBJECT, CLONE IT FIRST, THEN MUTATE AS YOU WISH.
	 */

	// UNIQUE IDENTIFIER(This is uniquely generated)
	private UUID TurretUUID;

	// ENTITIES UUIDs
	private UUID dummyGuardianUUID;
	private UUID dummyArmorStandUUID;

	// ENTITIES
	private Creature dummyPassenger;
	private ArmorStand turretStand;

	// OTHERS
	private Turret.Type type;
	private Double damage;
	private Integer speed;
	private Double health;
	//private Effect particalAnimation;
	//private Long particalRepeat;
	private Short blockTypeMeta;
	private Integer blockTypeId;
	private String turrentName;
	private List<String> lores;
	private Integer repairTime;
	//private EntityType target;
	private Integer purchaseCost;
	private boolean isAlive = false;
	private boolean exists = false;
	private int taskID = 0;
	
	private int attackTimer = 0;

	//******************************************************************\
	//  Constructor
	//******************************************************************\
	public TurretObject(){}
	public TurretObject(List<Guardian> guardians, List<ArmorStand> armorStands){

	}

	//******************************************************************\
	//  TurretUUID get; set;
	//******************************************************************\
	public UUID getTurretUUID() {
		return TurretUUID;
	}

	public void setTurretUUID(UUID turretUUID) {
		this.TurretUUID = turretUUID;
	}

	//******************************************************************\
	//  dummyArmorStandUUID get; set;
	//******************************************************************\
	public UUID getDummyArmorStandUUID() { return dummyArmorStandUUID; }

	public void setDummyArmorStandUUID(UUID dummyArmorStandUUID) { this.dummyArmorStandUUID = dummyArmorStandUUID; }

	//******************************************************************\
	//  dummyGuardianUUID get; set;
	//******************************************************************\
	public UUID getDummyPassengerUUID() { return dummyGuardianUUID; }

	public void setDummyPassengerUUID(UUID dummyGuardianUUID) { this.dummyGuardianUUID = dummyGuardianUUID; }

	//******************************************************************\
	//  TurretStand get; set;
	//******************************************************************\
	public ArmorStand getTurretStand() {
		return turretStand;
	}

	public void setTurretStand(ArmorStand turretStand) {
		this.turretStand = turretStand;
	}


	//******************************************************************\
	//  guardianUUID get; set;
	//******************************************************************\
	public Creature getDummyPassenger() {
		return dummyPassenger;
	}

	public void setDummyPassenger(Creature entity) {
		System.out.println("dummyPassenger = " + entity);
		this.dummyPassenger = entity;
	}

	//******************************************************************\
	//  type get; set;
	//******************************************************************\
	public Turret.Type getType() {
		return type;
	}
	
	public void setType(Turret.Type type) {
		this.type = type;
	}
	
	//******************************************************************\
	//  isAlive get; set;
	//******************************************************************\
	public boolean isAlive() {
		return isAlive;
	}
	
	public void setIsAlive(boolean alive) {
		this.isAlive = alive;
	}
	
	//******************************************************************\
	//  damage get; set;
	//******************************************************************\
	public Double getDamage() {
		return damage;
	}

	public void setDamage(Double damage) {
		this.damage = damage;
	}

	//******************************************************************\
	//  speed get; set;
	//******************************************************************\
	public Integer getSpeed() {
		return speed;
	}

	public void setSpeed(Integer speed) {
		this.speed = speed;
	}

	//******************************************************************\
	//  health get; set;
	//******************************************************************\
	public Double getHealth() {
		return health;
	}

	public void setHealth(Double health) {
		this.health = health;
	}

	//******************************************************************\
	//  blockTypeMeta get; set;
	//******************************************************************\
	public Short getBlockTypeMeta() {
		return blockTypeMeta;
	}

	public void setBlockTypeMeta(Short blockTypeMeta) {
		this.blockTypeMeta = blockTypeMeta;
	}

	//******************************************************************\
	//  blockTypeId get; set;
	//******************************************************************\
	public Integer getBlockTypeId() {
		return blockTypeId;
	}

	public void setBlockTypeId(Integer blockTypeId) {
		this.blockTypeId = blockTypeId;
	}

	//******************************************************************\
	//  turrentName get; set;
	//******************************************************************\
	public String getTurrentName() {
		return turrentName;
	}

	public void setTurrentName(String turrentName) {
		this.turrentName = turrentName;
	}

	//******************************************************************\
	//  isActive get; set;
	//******************************************************************\
	public boolean exists() {
		return this.exists;
	}

	public void setExists(boolean exists) {
		this.exists = exists;
	}

	//******************************************************************\
	//  lores get; set;
	//******************************************************************\
	public List<String> getLores() {
		return lores;
	}

	public void setLores(List<String> lores) {
		this.lores = lores;
	}

	//******************************************************************\
	//  repairTime get; set;
	//******************************************************************\
	public Integer getRepairTime() {
		return repairTime;
	}

	public void setRepairTime(Integer repairTime) {
		this.repairTime = repairTime;
	}

	//******************************************************************\
	//  target get; set;
	//******************************************************************\
	public void target(LivingEntity target) {
		Guardian g = (Guardian)dummyPassenger;
		if(type.equals(Type.Laser))
			g.setTarget(target);
	}
//	public EntityType getTarget() {
//		return target;
//	}
//
//	public void setTarget(EntityType target) {
//		this.target = target;
//	}

	//******************************************************************\
	//  purchaseCost get; set;
	//******************************************************************\
	public Integer getPurchaseCost() {
		return purchaseCost;
	}

	public void setPurchaseCost(Integer purchaseCost) {
		this.purchaseCost = purchaseCost;
	}

	//******************************************************************\
	//  Helper Functions
	//******************************************************************\

	public TurretObject clone() {
		try {
			TurretObject newOBJ = (TurretObject)super.clone();
			newOBJ.setExists(false);
			newOBJ.setTurretUUID(UUID.randomUUID());
			return newOBJ;
		}
		catch (CloneNotSupportedException e) {
			AttackPlugin.halt(e);
			return null;
		}
	}
	
	public int getAttackTimer() {
		return attackTimer;
	}


	public void setAttackTimer(int attackTimer) {
		this.attackTimer = attackTimer;
	}

	/*public void runEntityLauncher(AttackPlugin plugin, final EntityType thrownEntity){
		final HashMap<UUID, Location> fromLocation = new HashMap<UUID, Location>();
		this.taskID = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {

			@Override
			public void run() {
				Creature creature = getDummyPassenger();
				if(creature != null && creature.getLocation().getChunk().isLoaded() && creature.getTarget() != null){
					Entity thrown = creature.getWorld().spawnEntity(creature.getEyeLocation(), thrownEntity);
					thrown.setVelocity(creature.getTarget().getLocation().toVector());
				}
			}
		}, 0L, 35L);
	}*/

	public void runCreatureDemobilizer(AttackPlugin plugin, final Location spawnedLocation){
		final HashMap<UUID, Location> fromLocation = new HashMap<UUID, Location>();
		if(this.getDummyPassenger() == null){ return; }

		this.taskID = Bukkit.getServer().getScheduler().scheduleAsyncRepeatingTask(plugin, new Runnable() {

			@Override
			public void run() {
				Creature creature = getDummyPassenger();
				if(creature != null && spawnedLocation.getChunk().isLoaded()){
					double x = spawnedLocation.getX() - spawnedLocation.getX();
					double y = spawnedLocation.getY() - spawnedLocation.getY();
					double z = spawnedLocation.getZ() - spawnedLocation.getZ();
					Vector velocity = new Vector(x, y, z).normalize().multiply(-5);
					creature.setVelocity(velocity);
				}
			}
		}, 0L, 20L);
	}

	public void runTurretHeadFollow(AttackPlugin plugin){
		final HashMap<UUID, Location> fromLocation = new HashMap<UUID, Location>();

		this.taskID = Bukkit.getServer().getScheduler().scheduleAsyncRepeatingTask(plugin, new Runnable() {
			@Override
			public void run() {
				if (fromLocation.containsKey(getTurretUUID())) {
					if (fromLocation.get(getTurretUUID()).getChunk().isLoaded() &&
							!fromLocation.get(getTurretUUID()).equals(getDummyPassenger().getEyeLocation())) {
						if (getDummyPassenger().getTarget() != null) {
							double pitch = Math.toRadians(getDummyPassenger().getEyeLocation().getPitch());
							double yaw = Math.toRadians(getDummyPassenger().getEyeLocation().getYaw());
							EulerAngle a = new EulerAngle(pitch, yaw, 0);
							getTurretStand().setHeadPose(a);
						}
					}
				} else {
					if (getTurretUUID() != null && getDummyPassenger() != null) {
						fromLocation.put(getTurretUUID(), getDummyPassenger().getEyeLocation());
					}
				}
			}
		}, 0L, 3L);
	}
}
