package com.hoodmc.attack.utils;

import java.net.URI;
import java.nio.file.*;
import java.util.*;

import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.enums.Turret;

import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.*;

import java.io.*;
import java.net.URL;
import java.util.Vector;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

/**
 * Created by deadlyscone on 11/7/2015.
 *
 * ///////////////////////////////////////////////////////////////
 * //
 * // Do not put non-portable functions inside this Util.
 * //
 * //////////////////////////////////////////////////////////////
 */
public class HelperUtil {


   /*
        This function will check to see if a given location is between two
        diagonal points of a cuboid.
    */
    public static boolean isInBounds(Location loc, Location l1, Location l2) {
        int x1 = Math.min(l1.getBlockX(), l2.getBlockX());
        int y1 = Math.min(l1.getBlockY(), l2.getBlockY());
        int z1 = Math.min(l1.getBlockZ(), l2.getBlockZ());
        int x2 = Math.max(l1.getBlockX(), l2.getBlockX());
        int y2 = Math.max(l1.getBlockY(), l2.getBlockY());
        int z2 = Math.max(l1.getBlockZ(), l2.getBlockZ());

        return loc.getBlockX() >= x1 && loc.getBlockX() <= x2 && loc.getBlockY() >= y1 &&
                loc.getBlockY() <= y2 && loc.getBlockZ() >= z1 && loc.getBlockZ() <= z2;
    }

    public static boolean entityWithinRadius(Entity entity, int radius){

        for(Entity nearEntity : entity.getNearbyEntities(radius, radius, radius)){
            if(nearEntity != null){
                return true;
            }
        }
        return false;
    }

    /*
        Attempts to construct an itemstack from a string representation split by a regular
        expression. Function will accept minimal itemData indices to return a valid itemstack.
        if it fails, it will return null.
    */
    @SuppressWarnings("deprecation")
	public static ItemStack toItemStack(String itemData, String regexSplit){
        if(itemData != null && !itemData.isEmpty()){
            String[] data = itemData.split(regexSplit);
            Integer id = null;
            Integer amt = null;
            Short dmg = null;
            Material mat = null;

            if(data.length > 0){
                if(Material.matchMaterial(data[0]) != null){
                    mat = Material.matchMaterial(data[0]);
                }else if(tryParseInt(data[0]) != null && Material.getMaterial(Integer.valueOf(data[0])) != null){
                    id = Integer.valueOf(data[0]);
                }
            }
            if(data.length > 1 && tryParseInt(data[1]) != null){ amt = Integer.valueOf(data[1]); }
            if(data.length > 2 && tryParseShort(data[2]) != null){ dmg = Short.valueOf(data[2]); }

            if((id != null || mat != null) && amt != null && dmg != null){
                return id == null ? new ItemStack(mat, amt, dmg) : new ItemStack(id, amt, dmg);
            }else if((id != null || mat != null) && amt != null){
                return id == null ? new ItemStack(mat, amt) : new ItemStack(id, amt);
            }else if((id != null || mat != null)){
                return id == null ? new ItemStack(mat, 1) : new ItemStack(id, 1);
            }
        }
        return null;
    }
    
    public static ItemStack getItem(String setItemString, String name, int amount, List<String> loreList){
      ItemStack item = null;
      if (setItemString.contains(":")){
        String[] split = setItemString.split(":");
        int setItem = Integer.parseInt(split[0]);
        int subtype = Integer.parseInt(split[1]);
        item = new ItemStack(setItem, amount, (short)subtype);
      }else{
        int setItem = Integer.parseInt(setItemString);
        item = new ItemStack(setItem, amount);
      }
      
      ItemMeta m = item.getItemMeta();
      System.out.println("I: " + item);
      System.out.println("M: " + m);
      System.out.println("NAME: " + name);
      if(name != null)
    	  m.setDisplayName(name);
      m.setLore(loreList);
      item.setItemMeta(m);
      return item;
    }
    
    public static boolean placeTurret(Location l, Player p, String t, AttackPlugin plugin){
        Turret.Type turretType = Turret.Type.asTurretType(t, false);
        if(turretType != null){
            Turret.SpawnStatus spawnStatus = TurretManager.spawnNewTurret(turretType, p.getUniqueId(), l, plugin);
            if(spawnStatus.equals(Turret.SpawnStatus.INSUFFICIENT_OBJECTS)){
                PluginLogger.sendPlayerError(p, "You do not have a " + t + " turret to spawn. Type /attack to buy one." );
                return false;
            }else if(spawnStatus.equals(Turret.SpawnStatus.ENTITY_NEARBY)){
                PluginLogger.sendPlayerError(p, "Unable to spawn turret because you are too close to another turret" );
                return false;
            }
            return true;
        }else
            PluginLogger.sendPlayerError(p, "Cannot find turret type called " + t + ".");
        return false;
    }

    /*
        [OVERLOAD] Attempts to create an itemstack with a display name.
    */
    public static ItemStack toItemStack(String itemData, String regex, String displayname){
       return setItemName(toItemStack(itemData, regex), displayname);
    }

    /*
        Will set an itemstack displayname
    */
    public static ItemStack setItemName(ItemStack item, String displayname){
        if(item != null && displayname != null && !displayname.isEmpty()){
            ItemMeta im = item.getItemMeta();
            im.setDisplayName(displayname);
            item.setItemMeta(im);
            return item;
        }
        return null;
    }

    /*
        Will set an itemstack displayname supporting chat colors
    */
    public static ItemStack setItemName(ItemStack item, String displayname, Character altCode){
        if(item != null && displayname != null && !displayname.isEmpty() && altCode != null){
            ItemMeta im = item.getItemMeta();
            im.setDisplayName(ChatColor.translateAlternateColorCodes(altCode, displayname));
            item.setItemMeta(im);
            return item;
        }
        return null;
    }

    /*
        Will TRY to parse and integer
    */
    public static Integer tryParseInt(String string){
        try {
            return Integer.valueOf(string);
        }catch (NumberFormatException e){
            return null;
        }
    }

    /*
        Will TRY to parse and short
    */
    public static Short tryParseShort(String string){
        try {
            return Short.valueOf(string);
        }catch (NumberFormatException e){
            return null;
        }
    }

    /*
        Returns an encoded string that appears invisible to the
        client.
    */
    public static String encodeItemData(String str){
        try {
            String hiddenData = "";
            for(char c : str.toCharArray()){
                hiddenData += "§" + c;
            }
            return hiddenData;
        }catch (Exception e){
            AttackPlugin.halt(e);
            return null;
        }
    }

    /*
        Capitalizes the first letter of each work seperated by a space
    */
    public static String capitalizeFirst(String str, String splitBy){
        String[] data = str.toLowerCase().split(splitBy);
        String returnString = "";
        if(data.length < 1){ returnString += splitBy; }
        for(String s : data){
           returnString += ( Character.toUpperCase(s.charAt(0)) + s.substring(1) + splitBy );
        }
        return returnString;
    }

    /*
        Decodes an encoded string
    */
    public static String decodeItemData(String str){
        try {
            String[] hiddenData = str.split("(?:\\w{2,}|\\d[0-9A-Fa-f])+");
            String returnData = "";
            if(hiddenData == null){
                hiddenData = str.split("§");
                for(int i = 0; i < hiddenData.length; i++){
                    returnData += hiddenData[i];
                }
                return returnData;
            }else{
                String[] d = hiddenData[hiddenData.length-1].split("§");
                for(int i = 1; i < d.length; i++){
                    returnData += d[i];
                }
                return returnData;
            }

        }catch (Exception e){
            AttackPlugin.halt(e);
            return null;
        }
    }

    /*
        Will TRY find the desired world.
    */
    public static World findWorld(String desiredWorld){
        for(World serverWorld : Bukkit.getServer().getWorlds()){
            if(serverWorld.getName().equalsIgnoreCase(desiredWorld)){ return serverWorld; };
        }
        return null;
    }

    /*
        Moves the specified entity along a vector.
     */
    public void moveTowardLocation(Entity entity, Location to, double speed){
        Location loc = entity.getLocation();
        double x = loc.getX() - to.getX();
        double y = loc.getY() - to.getY();
        double z = loc.getZ() - to.getZ();
        org.bukkit.util.Vector velocity = new org.bukkit.util.Vector(x, y, z).normalize().multiply(-speed);
        entity.setVelocity(velocity);
    }
    
    /*
    	Particle Stuff
    */

    public static void displayParticle(String effect, Location l, double radius, int speed, int amount){
    	displayParticle(effect, l.getWorld(), l.getX(), l.getY(), l.getZ(), radius, speed, amount);
    }
    
    private static void displayParticle(String effect, World w, double x, double y, double z, double radius, int speed, int amount){
		Location l = new Location(w, x, y, z);
		//System.out.println("V: " + getServer().getVersion());
		if(Bukkit.getServer().getVersion().contains("1.9")){
			if(radius == 0){
				ParticleEffects_1_9.sendToLocation(ParticleEffects_1_9.valueOf(effect), l, 0, 0, 0, speed, amount);
			}else{
				ArrayList<Location> ll = getArea(l, radius, 0.2);
				for(int i = 0; i < amount; i++){
			        int index = new Random().nextInt(ll.size());
			        ParticleEffects_1_9.sendToLocation(ParticleEffects_1_9.valueOf(effect), ll.get(index), 0, 0, 0, speed, 1);
			        ll.remove(index);
				}
			}
		}
    }

    /*
        ?
     */
    private static ArrayList<Location> getArea(Location l, double r, double t) {
        ArrayList<Location> ll = new ArrayList<Location>();
        for (double x = l.getX() - r; x < l.getX() + r; x = x + t)
            for (double y = l.getY() - r; y < l.getY() + r; y = y + t)
                for (double z = l.getZ() - r; z < l.getZ() + r; z = z + t)
                    ll.add(new Location(l.getWorld(), x, y, z));
        return ll;
    }


/**
 *  ///////////////////////////////////////////////////////////////////////////////////////////
 *
 * 		File Helper Functions
 *
 ** //////////////////////////////////////////////////////////////////////////////////////////
 */

    /*
        Writes an internal resource to the specified destination.
     */
    public static void exportResource(URL resourceURL, File dest){
        try {
            dest.createNewFile();
            BufferedReader in = new BufferedReader(new InputStreamReader(resourceURL.openStream()));
            BufferedWriter out = new BufferedWriter(new FileWriter(dest));
            String line = "";
            while((line = in.readLine()) != null){
                out.write(line);
                out.newLine();
                out.flush();
            }
            in.close();
            out.close();

        }catch(Exception e){
            AttackPlugin.halt(e);
        }
    }

    /*
        Copies a file into a JAR
     */
    private boolean copyFileToJar(File sourceJar, File fileToCopy, String destInJar, StandardCopyOption copyOption){
        Map<String, String> zip_properties = new HashMap<>();
        zip_properties.put("create", "false");
        zip_properties.put("encoding", "UTF-8");
        URI zip_disk = URI.create("jar:" + sourceJar.getAbsoluteFile().toURI().toString().replace("\\","/"));
        try (FileSystem zipfs = FileSystems.newFileSystem(zip_disk, zip_properties)) {
            //Path of where the file should be copied to
            Path ZipFilePath = zipfs.getPath(destInJar);
            Path addNewFile = Paths.get(fileToCopy.getPath());
            Files.copy(addNewFile,ZipFilePath, copyOption);
            return true;
        }catch (IOException ex){
            ex.printStackTrace();
            return false;
        }
    }

    /*
        Copies a destination JAR to a specified destination.
     */
    public static boolean copyJar(JarFile jarFile, File destDir){
        String fileName = jarFile.getName();
        String fileNameLastPart = fileName.substring(fileName.lastIndexOf(File.separator));

        File destFile = new File(destDir, fileNameLastPart);

        try{
            JarOutputStream jos = new JarOutputStream(new FileOutputStream(destFile));
            Enumeration<JarEntry> entries = jarFile.entries();

            while (entries.hasMoreElements()) {
                JarEntry entry = entries.nextElement();
                InputStream is = jarFile.getInputStream(entry);

                //jos.putNextEntry(entry);
                //create a new entry to avoid ZipException: invalid entry compressed size
                jos.putNextEntry(new JarEntry(entry.getName()));
                byte[] buffer = new byte[4096];
                int bytesRead = 0;
                while ((bytesRead = is.read(buffer)) != -1) {
                    jos.write(buffer, 0, bytesRead);
                }
                is.close();
                jos.flush();
                jos.closeEntry();
            }
            jos.close();
            return true;
        }catch (IOException ex){
            ex.printStackTrace();
            return false;
        }
    }

    public static JarFile fileToJarFile(File jarSource) throws IOException{
        return new JarFile(jarSource);
    }

    /**
     *  ///////////////////////////////////////////////////////////////////////////////////////////
     *
     * 		Player Retrieval Functions
     *
     ** //////////////////////////////////////////////////////////////////////////////////////////
     */

    public static Player getPlayerByUUID(UUID playerUUID){
        for(Player p : Bukkit.getServer().getOnlinePlayers()){
            if(p.getUniqueId().equals(playerUUID)){
                return p;
            }
        }
        return null;
    }
}
