package com.hoodmc.attack.utils;

import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.enums.ConsoleColor;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by deadlyscone on 11/8/2015.
 *
 * This class is meant to handle all logger formatting.
 */
public class PluginLogger {

    private static final String pluginName = "[HoodAttack] ";


    /**
     *  ///////////////////////////////////////////////////////////////////////////////////////////
     *
     * 		Console Messages
     *
     ** //////////////////////////////////////////////////////////////////////////////////////////
     */
    public static void info(String msg){
        System.out.println(pluginName + msg);
    }
    public static void severe(String msg){
        System.out.println(ConsoleColor.LIGHT_RED + pluginName + msg + ConsoleColor.RESET);
    }
    public static void warning(String msg){
        System.out.println(ConsoleColor.YELLOW + pluginName + msg + ConsoleColor.RESET);
    }

    public static void log(String msg){
        System.out.println(ConsoleColor.DARK_CYAN + pluginName + msg + ConsoleColor.RESET);
    }

    /**
     *  ///////////////////////////////////////////////////////////////////////////////////////////
     *
     * 		Player Messages
     *
     ** //////////////////////////////////////////////////////////////////////////////////////////
     */
    public static void sendPlayerError(Player player, String message){
        player.sendMessage(ChatColor.RED + pluginName + " " + message);
    }
    public static void sendPlayerSuccess(Player player, String message){
        player.sendMessage(ChatColor.GREEN + pluginName + " " + message);
    }
    public static void sendPlayerWarning(Player player, String message){
        player.sendMessage(ChatColor.GOLD + pluginName + " " + message);
    }
    public static void sendPlayerInfo(Player player, String message){
        player.sendMessage(ChatColor.GRAY + pluginName + " " + message);
    }

    /**
     *  ///////////////////////////////////////////////////////////////////////////////////////////
     *
     * 		Other Messages
     *
     ** //////////////////////////////////////////////////////////////////////////////////////////
     */
    public static void showCommandHelp(CommandSender sender, AttackPlugin plugin){
        if(sender instanceof Player){
            Player player = ((Player)sender);
            String authors = "";
            for(String auth : plugin.getDescription().getAuthors()){authors += (auth + ", ");}
            player.sendMessage(ChatColor.DARK_AQUA+"_________________["+ plugin.getName() +"]_________________");
            player.sendMessage(ChatColor.AQUA + "Version: " + plugin.getDescription().getVersion());
            player.sendMessage(ChatColor.AQUA + "Created by: " + authors);
            player.sendMessage(ChatColor.AQUA + "______________________________________________");
            if (sender.hasPermission("admin.commands")){
                player.sendMessage(ChatColor.GOLD + "---------------[AttackCommands]---------------");
                player.sendMessage(ChatColor.RED + "/template reload  -  " + ChatColor.GREEN + "Reloads the plugin data and config.");
            }
        }else{
            System.out.println("["+ plugin.getName() +"] AttackCommands: /template reload");
        }
    }

}
