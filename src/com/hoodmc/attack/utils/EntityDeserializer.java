package com.hoodmc.attack.utils;

import com.hoodmc.attack.obj.TurretObject;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Guardian;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by deadlyscone on 11/16/2015.
 */
public class EntityDeserializer {

    private List<UUID> UUIDs = new ArrayList<>();
    private ArmorStand armorStand;
    private Creature creature;

    //******************************************************************\
    //  Constructor
    //******************************************************************\
    EntityDeserializer(TurretObject turretObject){
        Entity entity1 = getEntityByUUID(turretObject.getDummyArmorStandUUID());
        Entity entity2 = getEntityByUUID(turretObject.getDummyPassengerUUID());
        if (entity1 != null) { populateLists(entity1); turretObject.setTurretStand(this.armorStand);}
        if (entity2 != null) { populateLists(entity2); turretObject.setDummyPassenger(this.creature);}
    }

    /**
     *  ///////////////////////////////////////////////////////////////////////////////////////////
     *
     * 		Getters
     *
     ** //////////////////////////////////////////////////////////////////////////////////////////
     */

    public ArmorStand getArmorStands() {
        return armorStand;
    }

    public Creature getCreatures() {
        return creature;
    }

    /**
     *  ///////////////////////////////////////////////////////////////////////////////////////////
     *
     * 		Helper Functions
     *
     ** //////////////////////////////////////////////////////////////////////////////////////////
     */

    /*
        Searches for a UUID and will associate the entity by its type.
        Once associated, will be added to its respective list.
     */
    private Entity getEntityByUUID(UUID uid){
        for(World world : Bukkit.getServer().getWorlds()){
            for(Entity ent : Bukkit.getServer().getWorld(world.getUID()).getEntities()){
                if(ent.getUniqueId().equals(uid)){
                    return ent;
                }
            }
        }
        return null;
    }

    /*
        Adds the entity to a list containing its respective entity type.
     */
    private void populateLists(Entity entity){
        switch (entity.getType()){

            case DROPPED_ITEM:
                break;
            case EXPERIENCE_ORB:
                break;
            case LEASH_HITCH:
                break;
            case PAINTING:
                break;
            case ARROW:
                break;
            case SNOWBALL:
                break;
            case FIREBALL:
                break;
            case SMALL_FIREBALL:
                break;
            case ENDER_PEARL:
                break;
            case ENDER_SIGNAL:
                break;
            case THROWN_EXP_BOTTLE:
                break;
            case ITEM_FRAME:
                break;
            case WITHER_SKULL:
                break;
            case PRIMED_TNT:
                break;
            case FALLING_BLOCK:
                break;
            case FIREWORK:
                break;
            case ARMOR_STAND:
                this.armorStand = ((ArmorStand) entity);
            case MINECART_COMMAND:
                break;
            case BOAT:
                break;
            case MINECART:
                break;
            case MINECART_CHEST:
                break;
            case MINECART_FURNACE:
                break;
            case MINECART_TNT:
                break;
            case MINECART_HOPPER:
                break;
            case MINECART_MOB_SPAWNER:
                break;
            case CREEPER:
                break;
            case SKELETON:
                break;
            case SPIDER:
                break;
            case GIANT:
                break;
            case ZOMBIE:
                break;
            case SLIME:
                break;
            case GHAST:
                break;
            case PIG_ZOMBIE:
                break;
            case ENDERMAN:
                break;
            case CAVE_SPIDER:
                break;
            case SILVERFISH:
                break;
            case MAGMA_CUBE:
                break;
            case ENDER_DRAGON:
                break;
            case WITHER:
                break;
            case BAT:
                break;
            case WITCH:
            case ENDERMITE:
            case GUARDIAN:
            case BLAZE:
                this.creature = ((Creature)entity);
                break;
            case PIG:
                break;
            case SHEEP:
                break;
            case COW:
                break;
            case CHICKEN:
                break;
            case SQUID:
                break;
            case WOLF:
                break;
            case MUSHROOM_COW:
                break;
            case SNOWMAN:
                break;
            case OCELOT:
                break;
            case IRON_GOLEM:
                break;
            case HORSE:
                break;
            case RABBIT:
                break;
            case VILLAGER:
                break;
            case ENDER_CRYSTAL:
                break;
            case SPLASH_POTION:
                break;
            case EGG:
                break;
            case FISHING_HOOK:
                break;
            case LIGHTNING:
                break;
            case WEATHER:
                break;
            case PLAYER:
                break;
            case COMPLEX_PART:
                break;
            case UNKNOWN:
                break;
        }
    }

    /*
        Clears out all stored variables to minimize memory leaks.
     */
    public void dispose(){
        this.armorStand = null;
        this.creature = null;
        this.UUIDs.clear();
    }
}
