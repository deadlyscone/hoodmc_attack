package com.hoodmc.attack.utils;

import com.hoodmc.attack.enums.AttackerType;
import com.hoodmc.attack.enums.GUIViewAction;
import com.hoodmc.attack.enums.GUIViewType;
import com.hoodmc.attack.obj.AttackerObject;
import com.hoodmc.attack.obj.GUIObject;
import com.hoodmc.attack.obj.TurretObject;

import java.util.Arrays;

import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import com.hoodmc.attack.Attack;
import com.hoodmc.attack.AttackPlugin;

public class GUIManager{

	//******************************************************************\
	//  Creates and Displays the GUI
	//******************************************************************\
	public static void findAndDisplayGUI(GUIViewType type, Player player){
		for(GUIObject obj : DataManager.GUIObjects){
			if(obj != null && obj.getType() != null && obj.getType() == type){
				obj.displayGUI(player);
				break;
			}
		}
	}

	public static void handleGUIAction(GUIViewAction action, String itemName, Player player){
		Double playerBalance = DependencyManager.economy.getBalance(player);

		switch(action){
			case BUY_TURRET:
				System.out.println("1: " + itemName);
				System.out.println("2: " + TurretManager.getParentObject(itemName));
				TurretObject obj = TurretManager.getParentObject(itemName).clone();
				double cost = obj.getPurchaseCost();
				if(playerBalance >= cost){
					DependencyManager.economy.withdrawPlayer(player, cost);
					TurretManager.givePlayerNewTurret(player.getUniqueId(), obj);
					System.out.println("Bought a turret: " + DataManager.PlayerTurretObjects.get(player.getUniqueId()).size());
					//Add Turret Item
					player.getInventory().addItem(HelperUtil.getItem(obj.getBlockTypeId()+"", "§c§l" + obj.getType().toString().toLowerCase() + " turret", 1, Arrays.asList("Right click to place", "Can only be placd once")));
				}else{
					player.closeInventory();
					PluginLogger.sendPlayerError(player, "You do not have the funds required to buy this turret.");
				}
				break;
			case BUY_ATTACKER:
				AttackerType attackerType = AttackerType.fromString(itemName);

				if(attackerType != null){
					AttackerObject attackerObject = AttackerManager.getParentObject(attackerType);
					System.out.println("TYPE: " + attackerType);
					if(attackerObject != null && playerBalance >= attackerObject.getPurchaseCost()){
						try{
							System.out.println("BUY");
							AttackerManager.givePlayerNewAttacker(player, attackerType);
							PluginLogger.sendPlayerSuccess(player, "You successfully bought a " + attackerType + " attacker.");
						}catch(Exception x){x.printStackTrace();}
					}else{
						player.closeInventory();
						PluginLogger.sendPlayerError(player, "You do not have the funds required to buy this attacker.");
					}
				}else{
					PluginLogger.severe("Invalid configuration value " + itemName + ", please check your config.");
				}
				break;
			case ATTACK:
				attackCartel(player);
				break;
		}
	}
	
	private static void attackCartel(Player p){
		if(DataManager.getAttack(p) == null){
			String auuid = null;
			int i = 0;
			do{
				String id = DependencyManager.getCartels().getRandomCartelUUID(p);
				if(!DataManager.cartelUnderAttack(auuid))
					auuid = id;
				i =  + 1;
			}while(i < 999 && auuid == null);
			if(auuid != null){
				DataManager.currentAttacks.add(new Attack(p, auuid));
			}else
				PluginLogger.sendPlayerError(p, "All the cartels are already under attack!");
		}else
			PluginLogger.sendPlayerError(p, "You are already attacking a cartel!");
	}

	//******************************************************************\
	//  Translates a given string into either a ViewType or ViewAction
	//******************************************************************\
	@SuppressWarnings("rawtypes")
	public static Enum translateEnumFromString(String enumAsString, boolean halt){
		GUIViewAction action = GUIViewAction.tryParse(enumAsString);
		GUIViewType type = GUIViewType.tryParse(enumAsString);

		if(action != null){ return action; }
		else if(type != null){ return type; }

		if(halt){ AttackPlugin.halt(new Exception("TranslationNullException")); }
		return null;
	}
}
