package com.hoodmc.attack.utils;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.hoodmc.attack.AttackPlugin;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPCRegistry;
import net.milkbowl.vault.economy.Economy;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

import io.hotmail.com.jacob_vejvoda.Cartels.Cartels;

/**
 * Created by deadlyscone on 11/8/2015.
 */
public class DependencyManager {
    private AttackPlugin plugin;
    public static Economy economy;
    public static ProtocolManager protocolManager;

    public DependencyManager(AttackPlugin plugin){
        this.plugin = plugin;
    }

    public void setupEconomy() {
        RegisteredServiceProvider<Economy> economyProvider = plugin.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }else{
            AttackPlugin.halt(new Exception("DependencyNotFoundException [Vault] \n Perhaps you don't have a valid economy plugin?"));
        }
    }

    public void setupProtocolLib(){
        try{
            protocolManager = ProtocolLibrary.getProtocolManager();
        }catch (NoClassDefFoundError ex){
            AttackPlugin.halt(new Exception("DependencyNotFoundException: [ProtocolLib]"));
        }
    }

    public void checkCitizens(){

        try{
            NPCRegistry registry = CitizensAPI.getNPCRegistry();
            registry.deregisterAll();
        }catch (Exception e){
            AttackPlugin.halt(new Exception("DependencyNotFoundException: [Citizens]"));
        }
    }
    
    public static Cartels getCartels(){
    	return (Cartels) Bukkit.getServer().getPluginManager().getPlugin("Cartels");
    }
}
