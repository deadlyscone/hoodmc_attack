package com.hoodmc.attack.utils;

import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.enums.AttackerType;
import com.hoodmc.attack.obj.AttackerObject;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.npc.NPCRegistry;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by deadlyscone on 11/12/2015.
 * Edited by Eliminator on 01/21/2016
 */
public class AttackerManager {


	//******************************************************************\
	//  Get or set attacker count
	//******************************************************************\
	public static void givePlayerNewAttacker(Player owner, AttackerType type){
		System.out.println("Give A");
		int i = getAttackerAmount(owner, type);
		AttackPlugin.saveFile.set("attackers."+owner.getUniqueId().toString()+"."+type, i+1);
	    try{
	    	AttackPlugin.saveFile.save(AttackPlugin.saveYML);
	    }catch (IOException localIOException) {}
		//DataManager.getSaves().set("attackers."+owner.getUniqueId().toString()+"."+type, i+1);
		//DataManager.saveSaves();
    }
	
	public static void setPlayerAttackers(Player owner, AttackerType type, int i){
		AttackPlugin.saveFile.set("attackers."+owner.getUniqueId().toString()+"."+type, i);
	    try{
	    	AttackPlugin.saveFile.save(AttackPlugin.saveYML);
	    }catch (IOException localIOException) {}
    }
	
	public static int getAttackerAmount(Player owner, AttackerType type){
		return AttackPlugin.saveFile.getInt("attackers."+owner.getUniqueId().toString()+"."+type);
	}

	//******************************************************************\
	//  Spawns the NPC attached to that obj and removes the
	//  DataManagers stored instance for that player
	//******************************************************************\
	public static AttackerObject spawnAttackerFromObj(Player owner, Location loc, AttackerObject obj){
		NPCRegistry registry = CitizensAPI.getNPCRegistry();
		NPC npc = registry.createNPC(obj.getMobType(), obj.getName());
		obj.setNpcUUID(npc.getUniqueId());
		System.out.println("Attacker Classes."+obj.getType()+".Health:" + DataManager.getAttackerConfig().getString("Attacker Classes."+obj.getType()+".Health"));
		obj.setHealth(DataManager.getAttackerConfig().getString("Attacker Classes."+obj.getType()+".Health"));
		//if(npc != null && npc.getEntity() instanceof Damageable){
		//	((Damageable)npc.getEntity()).setHealth(obj.getHealth());
		//}
		npc.spawn(loc);
		obj.setExists(true); // VERY IMPORTANT!!!
		addPlayerAttacker(owner, obj);
		return obj;
	}


	//******************************************************************\
	//  Adds the given attacker object to the list of player attackers
	//******************************************************************\
	public static void storeNewPlayerAttacker(UUID uid, AttackerObject obj) {
		List<AttackerObject> playerObjList = DataManager.PlayerAttackerObjects.get(uid);
		if (playerObjList == null) {
			playerObjList = new ArrayList<AttackerObject>();
		}
		playerObjList.add(obj);
		DataManager.PlayerAttackerObjects.put(uid, playerObjList);
	}

	//******************************************************************\
	// Find a Player Owned AttackerObject by AttackerType
	//******************************************************************\
	public static AttackerObject getPlayerOwnedObject(AttackerType type, Player player){
		if(player != null && type != null){
			List<AttackerObject> playerOwnedObjs = DataManager.PlayerAttackerObjects.get(player.getUniqueId());
			if(playerOwnedObjs != null){
				for(AttackerObject obj : playerOwnedObjs){
					if(obj != null && obj.getType() != null && obj.getType() == type && !obj.exists()){
						return obj;
					}
				}
			}
		}
		return null;
	}

	//******************************************************************\
	// Find a Parent AttackerObject by AttackerType
	// WARNING!!! DO NOT MUTATE THESE PARENT OBJECTS.
	//******************************************************************\
	public static AttackerObject getParentObject(AttackerType type){
		if(type != null){
			List<AttackerObject> parentObjects = DataManager.AttackerObjects;
			if(parentObjects != null){
				for(AttackerObject obj : parentObjects){
					if(obj != null && obj.getType() != null && obj.getType() == type){
						return obj;
					}
				}
			}
		}
		return null;
	}

	//******************************************************************\
	//  ?
	//******************************************************************\
    public static void shootBeam(AttackerObject start, Entity target, final String effect, double speed){
		Location eyeLoc = start.getNPC().getEntity().getLocation();
		eyeLoc.setY(eyeLoc.getY()+1);
		double px = eyeLoc.getX();
		double py = eyeLoc.getY();
		double pz = eyeLoc.getZ();
		double yaw  = Math.toRadians(eyeLoc.getYaw() + 90);
		double pitch = Math.toRadians(eyeLoc.getPitch() + 90);
		double x = Math.sin(pitch) * Math.cos(yaw);
		double y = Math.sin(pitch) * Math.sin(yaw);
		double z = Math.cos(pitch);
		double distance = start.getNPC().getEntity().getLocation().distance(target.getLocation());
		for (int i = 1 ; i <= DataManager.AttackerScanRange ; i++) {
			final Location loc = new Location(eyeLoc.getWorld(), px + (i * x), py + (i * z), pz + (i * y));
			if(start.getNPC().getEntity().getLocation().distance(loc) <= distance){
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(DataManager.getPlugin(), new Runnable() {
				     public void run() {
						HelperUtil.displayParticle(effect, loc,0.3,0,5);
					 }
				}, (i));
			}else
				break;
		}
    }

	public static void addPlayerAttacker(Player p, AttackerObject obj){
		List<AttackerObject> l = new ArrayList<AttackerObject>();
		if(DataManager.PlayerAttackerObjects.get(p.getUniqueId()) != null)
			l = DataManager.PlayerAttackerObjects.get(p.getUniqueId());
		l.add(obj);
		DataManager.PlayerAttackerObjects.put(p.getUniqueId(), l);
	}
	// You can simplify the above function to this.
	/*public static void addPlayerAttacker(Player player, AttackerObject attackerObject){
		List<AttackerObject> currentPlayerObjs = new ArrayList<AttackerObject>(DataManager.PlayerAttackerObjects.get(player.getUniqueId()));
		if(currentPlayerObjs != null && !currentPlayerObjs.isEmpty()){
			DataManager.PlayerAttackerObjects.get(player.getUniqueId()).addAll(currentPlayerObjs);
		}
	}*/

	public static void removePlayerAttacker(Player p, AttackerObject obj){
		List<AttackerObject> l = new ArrayList<AttackerObject>();
		if(DataManager.PlayerAttackerObjects.get(p.getUniqueId()) != null)
			l = DataManager.PlayerAttackerObjects.get(p.getUniqueId());
		l.remove(obj);
		DataManager.PlayerAttackerObjects.put(p.getUniqueId(), l);
	}
	
	public static AttackerObject getAttacker(Entity e){
        for (Map.Entry<UUID, List<AttackerObject>> i : DataManager.PlayerAttackerObjects.entrySet())
            for(AttackerObject obj : i.getValue())
            	if(obj != null && obj.exists() && obj.getNPC() != null && obj.getNPC().getEntity().equals(e))
            		return obj;
        return null;
	}

	/*    public static AttackerObject findAttackerObj(String name){
        for(AttackerObject obj : DataManager.AttackerObjects){
            if(obj.getName().equalsIgnoreCase(name)){
                return obj;
            }
        }
        return null;
    }

    public static AttackerObject findAttackerObj(AttackerType type){
        for(AttackerObject obj : DataManager.AttackerObjects){
            if(obj.getType().equals(type)){
                return obj;
            }
        }
        return null;
    }*/
}
