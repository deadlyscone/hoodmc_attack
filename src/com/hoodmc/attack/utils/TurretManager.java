package com.hoodmc.attack.utils;

import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.enums.Turret;
import com.hoodmc.attack.obj.TurretObject;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by deadlyscone on 11/1/2015.
 */
public class TurretManager {

    //******************************************************************\
    //  This will store and spawn a cloned instance of the desired turret type
    //  NOTICE! USE THIS TO SPAWN NEW TURRETS
    //******************************************************************\
    public static Turret.SpawnStatus spawnNewTurret(Turret.Type type, UUID playerUUID, Location location, AttackPlugin plugin) {
        try {
            if (location != null) {
                Player player = HelperUtil.getPlayerByUUID(playerUUID);
                TurretObject PlayerTurret = getClonedObject(playerUUID, type);
                if (PlayerTurret != null && player != null && !HelperUtil.entityWithinRadius(player, 5)) {
                    PlayerTurret.setExists(true); //  VERY IMPORTANT!
                    PlayerTurret.setIsAlive(true);  //  VERY IMPORTANT!
                    System.out.println("Turret Properties."+type+".Speed: " + DataManager.getTurrentConfig().getString("Turret Properties."+type+".Speed"));
                    PlayerTurret.setSpeed(Integer.parseInt(DataManager.getTurrentConfig().getString("Turret Properties."+type+".Speed")));
                    //System.out.println("Turret Properties."+type+".Speed: " + DataManager.getTurrentConfig().getInt("Turret Properties."+type+".Speed"));
                    //PlayerTurret.setSpeed(DataManager.getTurrentConfig().getInt("Turret Properties."+type+".Speed"));
                    World world = player.getWorld();
                    //  Spawn entities
                    Location loc = location;
                    loc.setYaw(0);
                    ArmorStand turretStand = Bukkit.getServer().getWorld(location.getWorld().getUID()).spawn(
                            loc, ArmorStand.class);
                    Creature passenger = null;

                    switch (type){
                        case Laser:
                            passenger = world.spawn(location, Guardian.class);
                            turretStand.setPassenger(passenger);
                            break;

                        case Fireball:
                            passenger = world.spawn(location, Blaze.class);
                            turretStand.setPassenger(passenger);
                            break;

                        case Landmine:
                            passenger = world.spawn(location, Creeper.class);
                            turretStand.setPassenger(passenger);
                            break;
                        case Shade:
                            passenger = world.spawn(location, Zombie.class);
                            turretStand.setPassenger(passenger);
                            //PlayerTurret.runEntityLauncher(plugin, EntityType.WITHER_SKULL);
                            break;
                        case Grenade_Launcher:
                            passenger = world.spawn(location, Zombie.class);
                            turretStand.setPassenger(passenger);
                            break;
                    }


                    //  set properties
                    turretStand.setBasePlate(true);
                    turretStand.setVisible(false);
                    turretStand.setCustomName(ChatColor.translateAlternateColorCodes('&', PlayerTurret.getTurrentName()));
                    turretStand.setCustomNameVisible(true);
                    passenger.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 10));
                    passenger.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 10));
                    passenger.setRemoveWhenFarAway(false);
                    turretStand.setHelmet(new ItemStack(PlayerTurret.getBlockTypeId(), 1, PlayerTurret.getBlockTypeMeta()));
                    PlayerTurret.setDummyPassenger(passenger);
                    PlayerTurret.setTurretStand(turretStand);

                    // Runnable Tasks
                    PlayerTurret.runTurretHeadFollow(plugin);
                    return Turret.SpawnStatus.SPAWNED;
                }else if(PlayerTurret == null){
                    return Turret.SpawnStatus.INSUFFICIENT_OBJECTS;
                }else{
                    return Turret.SpawnStatus.ENTITY_NEARBY;
                }
            }
            return Turret.SpawnStatus.NOT_SPAWNED;
        } catch (Exception ex) {
            AttackPlugin.halt(ex);
            return Turret.SpawnStatus.NOT_SPAWNED;
        }
    }

    //******************************************************************\
    //  Adds the given turret object to the list of player turrets
    //******************************************************************\
    public static void givePlayerNewTurret(UUID uid, TurretObject obj) {
        List<TurretObject> playerObjList = DataManager.PlayerTurretObjects.get(uid);
        if (playerObjList == null) {
            playerObjList = new ArrayList<TurretObject>();
        }
        playerObjList.add(obj);
        DataManager.PlayerTurretObjects.put(uid, playerObjList);
    }

    //******************************************************************\
    //  Obtain the desired Parent TurretObject
    //******************************************************************\
    public static TurretObject getParentObject(String strType) {
        Turret.Type type = Turret.Type.asTurretType(strType, false);
        System.out.println("3: " + type);
        if (type != null) {
        	System.out.println("TOs1: " + DataManager.TurretObjects);
        	System.out.println("TOs2: " + DataManager.TurretObjects.size());
            for (TurretObject to : DataManager.TurretObjects) {
            	 System.out.println("4: " + to.getType());
                if (to.getType() == type) {
                    return to;
                }
            }
        }
        return null;
    }

    public static TurretObject getParentObject(Turret.Type type) {
        for (TurretObject to : DataManager.TurretObjects) {
            if (to.getType() == type) {
                return to;
            }
        }
        return null;
    }

    public static TurretObject getParentObject(Guardian guardian) {
    	for (Map.Entry<UUID,  List<TurretObject>> i : DataManager.PlayerTurretObjects.entrySet())
	        for (TurretObject to : i.getValue()) {
	        	//System.out.println("DP: " + to.getDummyPassenger());
	            if (to != null && to.getDummyPassenger() != null && to.getDummyPassenger().getUniqueId().equals(guardian.getUniqueId())) {
	                return to;
	            }
	        }
        return null;
    }

    //******************************************************************\
    //  Obtain the desired TurretObject for the player
    //******************************************************************\
    public static TurretObject getClonedObject(UUID playerUUID, Turret.Type type) {
        List<TurretObject> objList = DataManager.PlayerTurretObjects.get(playerUUID);
        if (objList != null) {
            for (TurretObject to : objList) {
                if (to.getType() == type && !to.exists()) {
                    return to;
                }
            }
        }
        return null;
    }

    //******************************************************************\
    //  Return the amount of objects a player currently has
    //******************************************************************\
    public static int getPlayerTurretCount(UUID playerUUID, Turret.Type type) {
        int count = 0;
        List<TurretObject> playerObjs= DataManager.PlayerTurretObjects.get(playerUUID);
        if(playerObjs != null){
            for(TurretObject obj : playerObjs){
                if(obj.getType().equals(type) && !obj.exists()){
                    count++;
                }
            }
        }
        return count;
    }

    /*public static TurretObject getClonedObject(Guardian guardian) {
        for (TurretObject to : DataManager.PlayerTurretObjects.get(playerUUID)) {
            if (to != null && to.getDummyPassenger() != null && to.getDummyPassenger().getUniqueId() ==
                    guardian.getUniqueId()) {
                return to;
            }
        }
        return null;
    }*/

    //******************************************************************\
    //  Remove a turret object from the player
    //******************************************************************\
    public static void removePlayerObject(TurretObject obj, UUID playerUUID) {
        DataManager.PlayerTurretObjects.get(playerUUID).remove(obj);

        if (DataManager.PlayerTurretObjects == null) {
            DataManager.PlayerTurretObjects = new HashMap<UUID, List<TurretObject>>();
        }
        if (DataManager.PlayerTurretObjects.get(playerUUID) == null) {
            DataManager.PlayerTurretObjects.put(playerUUID, new ArrayList<TurretObject>());
        }
    }
}
