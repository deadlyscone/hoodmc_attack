package com.hoodmc.attack.utils;

import java.io.*;
import java.util.*;
import java.util.Map.Entry;

import com.hoodmc.attack.enums.GUIViewType;
import com.hoodmc.attack.enums.InventorySize;
import com.hoodmc.attack.enums.Turret;
import com.hoodmc.attack.obj.GUIObject;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import com.hoodmc.attack.Attack;
import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.enums.AttackerType;
import com.hoodmc.attack.obj.AttackerObject;
import com.hoodmc.attack.obj.TurretObject;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class DataManager implements Serializable{
	private static final long serialVersionUID = -825676998082391488L;

	/**
	 * Files used
	 */
	private static final File pluginDataFolder = DataManager.getPluginDataFolder();
	private static final File pluginSaveFolder = DataManager.getSaveDataFolder();
	private static final File playerTurretObjectDat = DataManager.getSaveDataFile("pto");//the data file
	private static final File attConfigFile = DataManager.getConfigFile("AttackerConfig");
	private static final File turConfigFile = DataManager.getConfigFile("TurretConfig");
	private static final File configFile = DataManager.getConfigFile("Config");

	/**
	 * Loaded variables from configurations.
	 */
	public static boolean inDebug;
	public static List<AttackerObject> AttackerObjects = new ArrayList<AttackerObject>();
	public static int AttackerScanRange = 0;
	public static List<TurretObject> TurretObjects = new ArrayList<TurretObject>();
	public static List<GUIObject> GUIObjects = new ArrayList<GUIObject>();
	public static List<String> GUINames = new ArrayList<String>();

	/**
	 * Player Populated variables
	 */
	public static HashMap<UUID, Inventory> OpenGUIs = new HashMap<UUID, Inventory>();
	public static HashMap<UUID, List<TurretObject>> PlayerTurretObjects = new HashMap<UUID, List<TurretObject>>();
	public static HashMap<UUID, List<AttackerObject>> PlayerAttackerObjects = new HashMap<UUID, List<AttackerObject>>();

	/**
	 * Constants
	 */
	//  This is the character that configuration tags will be split by when encoding/decoding
	//  Item Data.
	public static final String SCHAR = "¦";


	/**
	 * Testing variables
	 */
	private static boolean dataWasLoaded = false;
	public static World TurretWorld;
	public static final String commandBase = "cartel";
	public static ArrayList<Attack> currentAttacks = new ArrayList<Attack>();


	/**
	 *  ///////////////////////////////////////////////////////////////////////////////////////////
	 *
	 * 		File Path/Helper Functions
	 *
	 ** //////////////////////////////////////////////////////////////////////////////////////////
	 */

		private static File getPluginDataFolder(){
			return Bukkit.getPluginManager().getPlugin("HoodAttack").getDataFolder();
		}
		
		private static File getConfigFile(String configName){
			return new File(getPluginDataFolder(), "/" + configName + ".yml");
		}

		private static File getSaveDataFolder(){
			return new File(getPluginDataFolder(), "/data");
		}

		private static File getSaveDataFile(String fileName){
			return new File(getSaveDataFolder(), "/" + fileName + ".dat");
		}

	//******************************************************************\
	//  Check Files
	//******************************************************************\
	public static void CheckFiles() {

		if (!pluginDataFolder.exists()){
			pluginDataFolder.mkdir();
		}
		if (!pluginSaveFolder.exists()){
			pluginSaveFolder.mkdir();
		}

		if(!playerTurretObjectDat.exists()){
			try{ playerTurretObjectDat.createNewFile(); }catch (Exception ex){ AttackPlugin.halt(ex); }
		}

		if(!attConfigFile.exists()){
			HelperUtil.exportResource(AttackPlugin.class.getResource("/AttackerConfig.yml"), attConfigFile);
		}
		if(!turConfigFile.exists()){
			HelperUtil.exportResource(AttackPlugin.class.getResource("/TurretConfig.yml"), turConfigFile);
		}
		if(!configFile.exists()){
			HelperUtil.exportResource(AttackPlugin.class.getResource("/Config.yml"), configFile);
			PluginLogger.warning("File Not Found!");
		}
	}

	/**
	 *  ///////////////////////////////////////////////////////////////////////////////////////////
	 *
	 * 		Convenience Helper Methods
	 *
	 ** //////////////////////////////////////////////////////////////////////////////////////////
	 */

	//******************************************************************\
	//  Reload Function
	//******************************************************************\
	public static void reloadData(AttackPlugin plugin){
		saveData();
		CheckFiles();
		loadConfigData();
		loadSavedData(plugin);
	}

	public static void ClearData(AttackPlugin plugin){
		System.out.println("Clearing Data...");
		plugin.getServer().getScheduler().cancelAllTasks();
		AttackerObjects.clear();
		TurretObjects.clear();
		TurretWorld = null;

		for (Entry<UUID, List<AttackerObject>> i : PlayerAttackerObjects.entrySet())
			for(AttackerObject obj : i.getValue())
				obj.delete();
		
		//  Will clear out all open GUI's
		Iterator it = OpenGUIs.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			((Inventory) pair.getValue()).clear();
			it.remove();
		}
	}


	/**
	 *  ///////////////////////////////////////////////////////////////////////////////////////////
	 *
	 * 		Loading Functions
	 *
	 ** //////////////////////////////////////////////////////////////////////////////////////////
	 */

		//******************************************************************\
		//  Loads Configuration values into memory
		//******************************************************************\
		@SuppressWarnings("unchecked")
		public static void loadConfigData(){
			if(AttackPlugin.isHalting){ return; }

			File customYmlSkills; 
			FileConfiguration config;
			String objClassPath = "";

			//******************************************************************\
			//  General Configuration
			//******************************************************************\
			customYmlSkills = getConfigFile("Config");
			config = YamlConfiguration.loadConfiguration(customYmlSkills);
			inDebug = config.getBoolean("Debug");

			try {
				//******************************************************************\
				//  Attacker Configuration
				//******************************************************************\
				customYmlSkills = getConfigFile("AttackerConfig");
				config = YamlConfiguration.loadConfiguration(customYmlSkills);
				objClassPath = "";

				AttackerScanRange = config.getInt(objClassPath + "Scan Radius");
				for (AttackerType type : AttackerType.values()) {
					AttackerObject obj = new AttackerObject();
					objClassPath = "Attacker Classes." + type.toString() + ".";

					obj.setType(type);
					obj.setHealth(config.getString(objClassPath + "Health"));
					obj.setName(config.getString(objClassPath + "Name"));
					obj.setEntityType(config.getString(objClassPath + "Mob Type"));
					obj.setPurchaseCost(config.getDouble(objClassPath + "Cost"));
					obj.setHelmet(config.getString(objClassPath + "Helmet"));
					obj.setChestplate(config.getString(objClassPath + "Chestplate"));
					obj.setLeggings(config.getString(objClassPath + "Leggings"));
					obj.setBoots(config.getString(objClassPath + "Boots"));
					obj.setWalkSpeed(config.getDouble(objClassPath + "Walk Speed"));
					obj.setFireRate(config.getInt(objClassPath + "Fire Rate"));
					//obj.setTargetsBeacon(config.getBoolean(objClassPath + "Target Beacon"));
					obj.setDamage(config.getDouble(objClassPath + "Damage"));
					obj.setParticle(config.getString(objClassPath + "Particle"));
					obj.setRange(type == AttackerType.Ranged ? config.getInt(objClassPath + "Range") : 0);
					AttackerObjects.add(obj);
				}

				//******************************************************************\
				//  Turret Configuration
				//******************************************************************\
				customYmlSkills = getConfigFile("TurretConfig");
				config = YamlConfiguration.loadConfiguration(customYmlSkills);

				TurretWorld = HelperUtil.findWorld(config.getString("World"));
				for (Turret.Type type : Turret.Type.values()) {
					TurretObject obj = new TurretObject();

					objClassPath = "Turret Properties." + Turret.Type.AsFString(type) + ".";
					obj.setBlockTypeId(config.getInt(objClassPath + "BlockID"));
					System.out.println(objClassPath + "BlockMeta" + " = " + config.getString(objClassPath + "BlockMeta"));
					obj.setBlockTypeMeta(Short.valueOf(config.getString(objClassPath + "BlockMeta")));
					obj.setTurretUUID(UUID.randomUUID());
					obj.setDamage(config.getDouble(objClassPath + "Damage"));
					obj.setHealth(config.getDouble(objClassPath + "Health"));
					obj.setRepairTime(config.getInt(objClassPath + "Repair TIme"));
					obj.setPurchaseCost(config.getInt(objClassPath + "Cost"));
					obj.setType(type);
					obj.setTurrentName(config.getString(objClassPath + "Name"));
					TurretObjects.add(obj);
				}
				System.out.println("TO: " + TurretObjects);
			}catch(Exception ex){
				AttackPlugin.halt(ex);
			}

			//******************************************************************\
			//  GUI Configuration Partial
			// (NOTE: Be sure turrets and attackers are loaded first)
			//******************************************************************\
			customYmlSkills = getConfigFile("Config");
			config = YamlConfiguration.loadConfiguration(customYmlSkills);
			try {
				for(GUIViewType type : GUIViewType.values()){
					GUIObject obj = new GUIObject();
					objClassPath = "GUI." + GUIViewType.asConfigString(type) + ".";

					obj.setName(config.getString(objClassPath + "Name"));
					obj.setSize(InventorySize.toInventorySize(config.getInt(objClassPath + "Rows")));
					obj.setFillItem(config.getString(objClassPath + "Fill_Item"));
					obj.setActionBlocks((List<String>) config.getList(objClassPath + "Items"));
					obj.setType(type);
					GUINames.add(ChatColor.translateAlternateColorCodes('&',obj.getName()));
					GUIObjects.add(obj);
				}

				}catch(Exception ex){
					AttackPlugin.halt(ex);
				}
		}
		
	public static YamlConfiguration getConfig(){
		return YamlConfiguration.loadConfiguration(getConfigFile("Config"));
	}
	
	public static YamlConfiguration getTurrentConfig(){
		return YamlConfiguration.loadConfiguration(getConfigFile("TurretConfig"));
	}
	
	public static YamlConfiguration getAttackerConfig(){
		return YamlConfiguration.loadConfiguration(getConfigFile("AttackerConfig"));
	}
	
//	public static YamlConfiguration getSaves(){
//		return YamlConfiguration.loadConfiguration(getConfigFile("saves"));
//	}
//	
//	public static void saveSaves(){
//		try {
//			File saveYML = new File(getPlugin().getDataFolder(), "saves.yml");
//			YamlConfiguration.loadConfiguration(getConfigFile("saves")).save(saveYML);
//		} catch (IOException e) {e.printStackTrace();}
//	}
	
//	public static void registerSaves(){
//		File saveYML = new File(getPlugin().getDataFolder(), "saves.yml");
//		//YamlConfiguration saveFile = YamlConfiguration.loadConfiguration(saveYML);
//	  	//Register Saves
//	      if (!saveYML.exists()) {
//	          try {
//	          	saveYML.createNewFile();
//	          } catch (IOException e) {
//	              e.printStackTrace();
//	          }
//	      }
//	}
//	
//	public static YamlConfiguration getSaves(){
//		File saveYML = new File(getPlugin().getDataFolder(), "saves.yml");
//		YamlConfiguration saveFile = YamlConfiguration.loadConfiguration(saveYML);
//		return saveFile;
//	}
//	
//	public static void saveSaves(){
//	    try{
//			File saveYML = new File(getPlugin().getDataFolder(), "saves.yml");
//			YamlConfiguration saveFile = YamlConfiguration.loadConfiguration(saveYML);
//	    	saveFile.save(saveYML);
//	    }catch (IOException localIOException) {}
//	}
//	
	public static AttackPlugin getPlugin(){
		return (AttackPlugin)Bukkit.getServer().getPluginManager().getPlugin("HoodAttack");
	}
	
	public static Attack getAttack(Player p){
		for(Attack a : currentAttacks)
			if(a.player.equals(p))
				return a;
		return null;
	}
	
	public static Attack getAttack(Chicken b){
		for(Attack a : currentAttacks)
			if(a.beacon.equals(b))
				return a;
		return null;
	}
	
	public static boolean cartelUnderAttack(String uuid){
		for(Attack a : currentAttacks)
			if(a.cartelUUID.equals(uuid))
				return true;
		return false;
	}
	
	public static void deleteGame(final Attack a){
	    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(DataManager.getPlugin(), new Runnable(){
	        public void run(){
	    		currentAttacks.remove(a);
	        }
	    }, 4L);
	}

	//******************************************************************\
	//  Loads Saved Data into memory
	//******************************************************************\
	public static void loadSavedData(AttackPlugin plugin){
		if(AttackPlugin.isHalting){ return; }

		HashMap<UUID, List<UUID>> uidLoadedMap = new HashMap<>();
		int spawnCount = 0;
		int notSpawnedCount = 0;
		try {
			//  Populate variable data from the data folder
			FileInputStream fis = new FileInputStream(playerTurretObjectDat);
			ObjectInputStream ois = new ObjectInputStream(fis);
			HashMap<UUID, List<TurretObject>> loadedMap = (HashMap<UUID, List<TurretObject>>) ois.readObject();
			ois.close();
			fis.close();
			Iterator it = loadedMap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				for(TurretObject obj : ((List<TurretObject>)pair.getValue())){
					EntityDeserializer es = new EntityDeserializer(obj);
					es.dispose();
					if(obj.isAlive()){
						spawnCount++; obj.runTurretHeadFollow(plugin);
						if(obj.getDummyPassenger() != null){
							obj.runCreatureDemobilizer(plugin, obj.getDummyPassenger().getLocation());
						}
					}else{notSpawnedCount++;}
				}
			}
			DataManager.PlayerTurretObjects = loadedMap;
			dataWasLoaded = true;
			PluginLogger.log("Loaded turret object(s) successfully. Alive("+spawnCount+") / Not Spawned("+notSpawnedCount+")");
		}catch (EOFException eof){
			dataWasLoaded = true;
		}catch (IOException ex){
			AttackPlugin.halt(ex);
		}catch (ClassNotFoundException ex){
			AttackPlugin.halt(ex);
		}
	}

	/**
	 *  ///////////////////////////////////////////////////////////////////////////////////////////
	 *
	 * 		Saving Functions
	 *
	 ** //////////////////////////////////////////////////////////////////////////////////////////
	 */
		public static void saveData(){
			if(!dataWasLoaded){ return; }
			/*
				Takes all objects and replaces the entity with a UUID to allow
				serialization of the object.
			 */
			ObjectOutputStream oos;
			Iterator it = PlayerTurretObjects.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();
				List<TurretObject> objs = ((List<TurretObject>)pair.getValue());
				for(TurretObject obj : objs){
					ArmorStand stand = obj.getTurretStand();
					Creature dummyPassenger = obj.getDummyPassenger();
					if(stand != null){ obj.setDummyArmorStandUUID(stand.getUniqueId()); }
					if(dummyPassenger != null){ obj.setDummyPassengerUUID(dummyPassenger.getUniqueId()); }
					obj.setDummyPassenger(null);
					obj.setTurretStand(null);
				}
			}
			try {
				//  Player Turret Objects
				FileOutputStream fos = new FileOutputStream(playerTurretObjectDat, false);
				oos = new ObjectOutputStream(fos);
				oos.writeObject(DataManager.PlayerTurretObjects);
				oos.flush();
				oos.close();
				fos.close();
				PluginLogger.log("Saved turret object data successfully.");
			} catch (IOException e) {
				AttackPlugin.halt(e);
			}
			for(Attack ca : currentAttacks)
				ca.endAttack();
		}
}
