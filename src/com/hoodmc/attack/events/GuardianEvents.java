package com.hoodmc.attack.events;

import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.obj.AttackerObject;
import com.hoodmc.attack.obj.TurretObject;
import com.hoodmc.attack.utils.AttackerManager;
import com.hoodmc.attack.utils.DataManager;
import com.hoodmc.attack.utils.PluginLogger;
import com.hoodmc.attack.utils.TurretManager;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Guardian;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.util.EulerAngle;

import java.util.*;

/**
 * Created by deadlyscone on 11/2/2015.
 */
public class GuardianEvents implements Listener {

    AttackPlugin plugin;

    //******************************************************************\
    //  Constructor
    //******************************************************************\
    public GuardianEvents(AttackPlugin plugin){
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.plugin = plugin;
        scan();
    }
    
    //******************************************************************\
    //  ?
    //******************************************************************\
    public void scan(){
        final int range = DataManager.AttackerScanRange;
        // Start Turret Manager Repeating Task
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){ public void run(){
        	for (Map.Entry<UUID,  List<TurretObject>> i : DataManager.PlayerTurretObjects.entrySet())
    	        for (TurretObject obj : i.getValue()) {
        			 if(obj != null && obj.isAlive() && obj.exists() && obj.getTurretStand() != null && obj.getDummyPassenger() != null){
        				 obj.setAttackTimer(obj.getAttackTimer() - 5);
        				 if(obj.getAttackTimer() <= 0){
        					 System.out.println("Speed: " + obj.getSpeed());
        					 obj.setAttackTimer(obj.getSpeed());
		        			 for(Entity e : obj.getTurretStand().getNearbyEntities(range, range, range)){
		        				 AttackerObject ao = AttackerManager.getAttacker(e);
		        				 //System.out.println("AO: " + ao);
		        				 if(ao != null && ao.getNPC().getEntity().getLocation().distance(obj.getDummyPassenger().getLocation()) > 2){
		        					 obj.target((LivingEntity)ao.getNPC().getEntity());
		        					 ao.setHealth(ao.getHealth()-((int)obj.getDamage().intValue()));
		        					 System.out.println("HP: " + ao.getHealth());
		        					 break;
		        				 }
		        			 }
        				 }
        			 }
	        	 }
        }}, 5, (20));
    }

    //******************************************************************\
    //  EntityDamageByEntityEvent
    //******************************************************************\
    /*@EventHandler(priority = EventPriority.HIGHEST)
    public void onAttackGuardian(EntityDamageByEntityEvent e){
        if (e.getEntity().getType() == EntityType.GUARDIAN){
            Guardian turretGuardian = ((Guardian)e.getEntity());
            if (TurretManager.getParentObject(turretGuardian) != null){
                e.setCancelled(true);
            }
        }
    }*/

    private void kill(final TurretObject tur){    	
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable(){
          public void run(){
          	tur.setIsAlive(false);
        	if(tur.getTurretStand() != null)
        		tur.getTurretStand().remove();
        	tur.setExists(false);
        	DataManager.TurretObjects.remove(tur);
          }
        }, 3L);
    }
    
    //******************************************************************\
    //  EntityDamageByEntityEvent
    //******************************************************************\
    @EventHandler(priority = EventPriority.NORMAL)
    public void onTurretDeath(EntityDeathEvent e){
    	System.out.println("Die");
    	System.out.println("E: " + e.getEntity().getType());
        if (e.getEntity().getType().equals(EntityType.GUARDIAN)){
            Guardian turretGuardian = ((Guardian)e.getEntity());
            TurretObject tur = TurretManager.getParentObject(turretGuardian);
            System.out.println("tur: " + tur);
            if (tur != null)
            	kill(tur);
        }
    }
    
    @EventHandler
    public void onTarget(EntityTargetEvent e){
    	//System.out.println("Tar");
    	//System.out.println("E: " + e.getEntity().getType());
    	//System.out.println("T: " + e.getTarget());
    	if(e.getEntity().getType().equals(EntityType.GUARDIAN))
	    	if(e.getTarget() instanceof Player){
	            Guardian turretGuardian = ((Guardian)e.getEntity());
	            TurretObject tur = TurretManager.getParentObject(turretGuardian);
	            System.out.println("tur: " + tur);
	            if (tur != null)
	            	e.setCancelled(true);
	    	}
    }
}
