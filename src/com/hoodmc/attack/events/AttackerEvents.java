package com.hoodmc.attack.events;


import com.hoodmc.attack.Attack;
import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.enums.AttackerType;
import com.hoodmc.attack.obj.AttackerObject;
import com.hoodmc.attack.utils.AttackerManager;
import com.hoodmc.attack.utils.DataManager;
import com.hoodmc.attack.utils.HelperUtil;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by deadlyscone on 11/21/2015.
 */
public class AttackerEvents implements Listener {

    AttackPlugin plugin;
    public AttackerEvents(AttackPlugin plugin){
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.plugin = plugin;
        scanTimer();
    }

    //******************************************************************\
    //  ?
    //******************************************************************\
    public void scanTimer(){
        // Start Attacker Manager Repeating Task
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable(){ public void run(){
       	 for (Map.Entry<UUID, List<AttackerObject>> i : DataManager.PlayerAttackerObjects.entrySet())
             for(AttackerObject obj : i.getValue())
            	 scan(obj);
        }}, 5, (20));
    }
    
    public static void scan(AttackerObject obj){
        //final int range = DataManager.AttackerScanRange;
    	final int range = 35;
     	if(obj.exists()){
            	//Time
            	obj.setAttackTimer(obj.getAttackTimer() - 5);
            	//Death?
            	if(obj.getHealth() <= 0){
            		obj.kill(DataManager.getPlugin());
            		//LivingEntity le = obj.getNPC().getEntity().getWorld().spawnEntity(obj.getNPC().getEntity(), arg1)
            	}else if(obj.getAttackTimer() <= 0){
            		obj.setAttackTimer(obj.getFireRate());
                	//Attack
                    if(obj.getTarget() == null){
                    	//System.out.println("No target");
                        LivingEntity turret = null;
                        Chicken beacon = null;
                        double distance = DataManager.AttackerScanRange;

                        if(obj.getNPC() != null && obj.getNPC().getEntity() != null){
                            for(Entity e : obj.getNPC().getEntity().getNearbyEntities(range, range, range)){
                            	//System.out.println("Found: " + e.getType());
                            	//System.out.println("Pas: " + e.getPassenger());
                                if(e.getType().equals(EntityType.ARMOR_STAND) && e.getPassenger() != null && (e.getPassenger().getType().equals(EntityType.GUARDIAN))){
                                    LivingEntity gu = (LivingEntity) e.getPassenger();
                                    double far = obj.getNPC().getEntity().getLocation().distance(gu.getLocation());
                                    if(far < distance){
                                        distance = obj.getNPC().getEntity().getLocation().distance(gu.getLocation());
                                        turret = gu;
                                    }
                                }else if(e.getType().equals(EntityType.CHICKEN))
                                    beacon = (Chicken) e;
                            }
                        }

                        if(turret != null){
                            //Target Turret
                            obj.setTarget(turret);
                        }else if(beacon != null){
                        	obj.setTarget(beacon);
                            //obj.setTarget(beacon);
                        }
                    }else{
                        if(obj.getTarget().isDead()){
                            obj.clearTarget();
                        }else if(obj.getType().equals(AttackerType.Ranged)){
                            double distance = obj.getNPC().getEntity().getLocation().distance(obj.getTarget().getLocation());
                            if(distance < obj.getRange()){
                                //Attack
                            	obj.stopWalk();
                            	obj.setLookAt(obj.getTarget().getLocation());
                                AttackerManager.shootBeam(obj, obj.getTarget(), obj.getParticle(), obj.getFireRate());
                        		if(obj.getTarget() instanceof Chicken){
                        			Attack a = DataManager.getAttack((Chicken)obj.getTarget());
                        			if(a != null){
                        				a.setBeaconHP((int)(a.beaconHP - obj.getDamage()));
                        			}
                        		}else
                        			((Damageable)obj.getTarget()).damage(obj.getDamage());
                            }
                        }else if(obj.getType().equals(AttackerType.Melee)){
                            //Attack Melee
                    		if(obj.getTarget() instanceof Chicken){
                    			Attack a = DataManager.getAttack((Chicken)obj.getTarget());
                    			if(a != null){
                    				a.setBeaconHP((int)(a.beaconHP - obj.getDamage()));
                    			}
                    		}
//		                        	if(obj.targetLiving == false){
//		                        		if(obj.getTarget() != null){
//		                        			obj.getTarget().remove();
//		                                    Location l = obj.getNPC().getEntity().getLocation();
//		                                    HelperUtil.displayParticle("BIG_EXPLODE", l,0,1,1);
//		                                    l.getWorld().playSound(l, Sound.EXPLODE, 1, 1);
//		                        		}
//		                        	}
                        }else if(obj.getType().equals(AttackerType.Kamikaze)){
                            double distance = obj.getNPC().getEntity().getLocation().distance(obj.getTarget().getLocation());
                            if(distance <= 1){
                                //Blow Up
                                Location l = obj.getNPC().getEntity().getLocation();
                                HelperUtil.displayParticle("BIG_EXPLODE", l,0,1,1);
                                l.getWorld().playSound(l, Sound.ENTITY_GENERIC_EXPLODE, 1, 1);
                        		if(obj.getTarget() instanceof Chicken){
                        			Attack a = DataManager.getAttack((Chicken)obj.getTarget());
                        			if(a != null){
                        				a.setBeaconHP((int)(a.beaconHP - obj.getDamage()));
                        			}
                        		}else
                                	((Damageable)obj.getTarget()).damage(obj.getDamage());
                            }
                        }
                    }
            	}
     	}
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent e){
    	Player p = e.getPlayer();
		if(DataManager.cartelUnderAttack(p.getUniqueId().toString())){
			p.kickPlayer("Your cartel is under attack!");
		}
    }
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerQuit(PlayerQuitEvent e){
    	Player p = e.getPlayer();
    	Attack a = DataManager.getAttack(p);
		if(a != null){
			a.endAttack();
		}
    }
    
//    @EventHandler(priority = EventPriority.NORMAL)
//    public void onPlayerInteract(PlayerInteractEvent e){
//    	Player p = e.getPlayer();
//    	Attack a = DataManager.getAttack(p);
//		if(a != null && a.beaconHP > 0){
//			e.setCancelled(true);
//			PluginLogger.sendPlayerError(p, "You must destroy the beacon first!");
//		}
//    }
}
