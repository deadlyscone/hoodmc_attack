package com.hoodmc.attack.events;


import com.hoodmc.attack.Attack;
import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.enums.AttackerType;
import com.hoodmc.attack.enums.GUIViewAction;
import com.hoodmc.attack.enums.GUIViewType;
import com.hoodmc.attack.obj.AttackerObject;
import com.hoodmc.attack.utils.AttackerManager;
import com.hoodmc.attack.utils.DataManager;
import com.hoodmc.attack.utils.GUIManager;
import com.hoodmc.attack.utils.HelperUtil;
import com.hoodmc.attack.utils.PluginLogger;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;

/**
 * Created by deadlyscone on 11/8/2015.
 */
public class GUIEvents implements Listener {

    AttackPlugin plugin;

    //******************************************************************\
    //  Constructor
    //******************************************************************\
    public GUIEvents(AttackPlugin plugin){
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.plugin = plugin;
    }

    //******************************************************************\
    //  GUI Events
    //******************************************************************\
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onGUIClick(InventoryClickEvent e) {
    	System.out.println("C1");
        if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR &&
                DataManager.GUINames.contains(e.getInventory().getName()) && e.getView().getItem(e.getSlot()).equals(e.getCurrentItem())){
            Player player = e.getWhoClicked() instanceof Player ? (Player) e.getWhoClicked() : null;
            if(player == null){return;}


            String data = HelperUtil.decodeItemData(e.getCurrentItem().getItemMeta().getDisplayName());
            System.out.println(data);
            String itemType = "";
            String[] splitData = data.split(DataManager.SCHAR);
            if(splitData.length > 1){
                itemType = splitData[1];
                data = splitData[0];
            }

            Enum action = GUIManager.translateEnumFromString(data, false);
            if(action != null && action instanceof GUIViewAction){
                GUIManager.handleGUIAction(((GUIViewAction)action), itemType, player);
            }else if(action != null && action instanceof GUIViewType){
                GUIViewType type = GUIViewType.tryParse(action.toString());
                GUIManager.findAndDisplayGUI(type, player);
            }
            e.setCancelled(true);

        }else if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR &&
                DataManager.GUINames.contains(e.getInventory().getName()) && !e.getView().getItem(e.getSlot()).equals(e.getCurrentItem())){
            e.setCancelled(true);
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onBlockPlace(BlockPlaceEvent e) {
    	Player p = e.getPlayer();
    	try{
    		String in = p.getItemInHand().getItemMeta().getDisplayName();
    		if(in.contains("§c§l") && in.contains(" turret")){
    			e.setCancelled(true);
    			//Get Type
    			String t = in.replace("§c§l", "").replace(" turret", "");
    			//Place Turret
    			if(HelperUtil.placeTurret(e.getBlock().getLocation(), p, t, plugin)){
        			//Take Item
        			int a = p.getItemInHand().getAmount()-1;
        			if(a > 0){
        				p.getItemInHand().setAmount(a);
        			}else
        				p.setItemInHand(null);
    			}
    		}
    	}catch(Exception x){}
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerInteract(PlayerInteractEvent e) {
    	Player p = e.getPlayer();
    	try{
    		String in = p.getItemInHand().getItemMeta().getDisplayName();
    		if(in.contains("§2§l") && in.contains(" attacker")){
    			e.setCancelled(true);
    			//Get Type
    			String t = in.replace("§2§l", "").replace(" attacker", "");
    			//Place Turret
            	AttackerObject obj = null;
            	for(AttackerObject o : DataManager.AttackerObjects)
            		if(o != null && o.getType() != null && o.getType().equals(AttackerType.fromString(t)))
            			obj = o.clone();
            	if(obj != null){
            		Location l = p.getLocation();
            		Attack a = DataManager.getAttack(p);
            		if(a != null)
            			l = a.attackSpawn;
            		AttackerObject o = AttackerManager.spawnAttackerFromObj(p, l, obj);
            		AttackerEvents.scan(o);
            		//Take Item
        			int am = p.getItemInHand().getAmount()-1;
        			if(am > 0){
        				p.getItemInHand().setAmount(am);
        			}else
        				p.setItemInHand(null);
            		return;
            	}else
            		p.sendMessage("§cAttacker type invalid!");
    		}
    	}catch(Exception x){}
    	Attack a = DataManager.getAttack(p);
		if(a != null && a.beaconHP > 0){
			e.setCancelled(true);
			PluginLogger.sendPlayerError(p, "You must destroy the beacon first!");
		}
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onGUIClose(InventoryCloseEvent e) {
        Inventory inv = e.getInventory();
        if(e.getPlayer() instanceof Player && DataManager.GUINames.contains(e.getInventory().getName())){
            DataManager.OpenGUIs.entrySet().remove(((Player)e.getPlayer()));
        }
    }
}
