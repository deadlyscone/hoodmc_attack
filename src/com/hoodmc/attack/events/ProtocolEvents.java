package com.hoodmc.attack.events;

import org.bukkit.Sound;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.utils.DependencyManager;

/**
 * Created by deadlyscone on 11/9/2015.
 */
public class ProtocolEvents {

    //******************************************************************\
    //  Prevents the packet being sent to the client
    //******************************************************************\
    public ProtocolEvents(AttackPlugin plugin){
        DependencyManager.protocolManager.addPacketListener(
                new PacketAdapter(plugin, ListenerPriority.NORMAL, PacketType.Play.Server.NAMED_SOUND_EFFECT){
                    @Override
                    public void onPacketSending(PacketEvent e){
                        //Item packets (id: 0x29)
                    	//System.out.println("SE1: " + e.getPacket().getSoundEffects().getValues().get(0));
                    	//System.out.println("SE2: " + Sound.ENTITY_ELDER_GUARDIAN_AMBIENT.toString());
                    	if(e.getPacket().getSoundEffects().getValues().get(0).equals(Sound.ENTITY_GUARDIAN_AMBIENT.toString()))
                    		e.setCancelled(true);
//                        if(e.getPacket().getStrings().read(0).equalsIgnoreCase("mob.guardian.land.idle")){
//                            e.setCancelled(true);
//                        }
                    }
                });

    }
}
