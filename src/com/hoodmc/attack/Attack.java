package com.hoodmc.attack;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.hoodmc.attack.enums.AttackerType;
import com.hoodmc.attack.obj.AttackerObject;
import com.hoodmc.attack.utils.AttackerManager;
import com.hoodmc.attack.utils.DataManager;
import com.hoodmc.attack.utils.DependencyManager;
import com.hoodmc.attack.utils.HelperUtil;
import com.hoodmc.attack.utils.PluginLogger;

import net.milkbowl.vault.economy.Economy;

public class Attack {
	public Player player;
	public String cartelUUID;
	public Location attackSpawn;
	PreviousStuff pStuff;
	public LivingEntity beacon;
	public int beaconHP;

	public Attack(Player p, String auuid) {
		player = p;
		cartelUUID = auuid;
		//Get Spawn
		attackSpawn = DependencyManager.getCartels().getSpawn(auuid);
		YamlConfiguration c = DataManager.getConfig();
		attackSpawn.setX(attackSpawn.getX()+c.getDouble("attackSpawn.x"));
		attackSpawn.setY(attackSpawn.getY()+c.getDouble("attackSpawn.y"));
		attackSpawn.setZ(attackSpawn.getZ()+c.getDouble("attackSpawn.z"));
		pStuff = new PreviousStuff(p);
		int base = DataManager.getConfig().getInt("beaconHP");
		int lvl = DependencyManager.getCartels().getCarteLevel(cartelUUID);
		int ap = DataManager.getConfig().getInt("beaconHPLevelPercent");
		beaconHP = (int)(base+(((base/100.0)*ap)*lvl));
		startAttack();
	}
	
	private void startAttack(){
		PluginLogger.sendPlayerSuccess(player, "You have started the attack!");
		player.getInventory().clear();
		player.teleport(attackSpawn);
		//Get Beacon
		int r = 35;
		for(Entity e : player.getNearbyEntities(r, r, r))
			if(e instanceof Chicken){
				beacon = (LivingEntity) e;
				break;
			}
		//Give Attackers
		for(AttackerType at : AttackerType.values())
			if(at != null){
				//int i = AttackPlugin.saveFile.getInt("attackers."+player.getUniqueId().toString()+"."+at);
				int i = AttackerManager.getAttackerAmount(player, at);
				if(i > 0){
					//System.out.println("Attacker Classes."+at+".Item: " + DataManager.getAttackerConfig().getString("Attacker Classes."+at+".Item"));
					ItemStack s = HelperUtil.getItem(DataManager.getAttackerConfig().getString("Attacker Classes."+at+".Item"), "§2§l"+at+" attacker", i, Arrays.asList("Right click to place", "Will start at attack spawn"));
					player.getInventory().addItem(s);
					AttackerManager.setPlayerAttackers(player, at, 0);
				}
			}
	}
	
	public void endAttack(){
		//Save Unused Attackers
		for(ItemStack s : player.getInventory())
			if(s != null)
				try{
					String in = s.getItemMeta().getDisplayName();
		    		if(in.contains("§2§l") && in.contains(" attacker")){
		    			String t = in.replace("§2§l", "").replace(" attacker", "");
		    			AttackerType at = AttackerType.valueOf(t);
		    			AttackerManager.setPlayerAttackers(player, at,  AttackerManager.getAttackerAmount(player, at) + 1);
		    		}
				}catch(Exception x){}
		pStuff.restore();
        for (Map.Entry<UUID, List<AttackerObject>> i : DataManager.PlayerAttackerObjects.entrySet())
            for(AttackerObject obj : i.getValue())
            	if(obj.exists()){
            		obj.kill(DataManager.getPlugin());
            	}
		DataManager.PlayerAttackerObjects.remove(player.getUniqueId());
		player.sendMessage("§4[§b*§4] §7Attack completed!");
		//Restore Beacon
		if(beaconHP <= 0 && beacon.isDead())
			DependencyManager.getCartels().generateBeacon(cartelUUID);
		//Remove Attack
		System.out.println("IE: " + DataManager.getPlugin().isEnabled());
		if(DataManager.getPlugin().isEnabled())
			DataManager.deleteGame(this);
	}
	
	@SuppressWarnings("deprecation")
	public void setBeaconHP(int i){
		System.out.println("beaconHP: " + beaconHP);
		System.out.println("Set: " + i);
		beaconHP = i;
		if(beaconHP <= 0){
            Location l = beacon.getLocation();
            HelperUtil.displayParticle("BIG_EXPLODE", l,0,1,1);
            l.getWorld().playSound(l, Sound.ENTITY_GENERIC_EXPLODE, 1, 1);
            beacon.remove();
            player.sendMessage("§4[§b*§4] §7You have won the battle, you have 1 minute to loot chests!");
            //Money
            Economy eco = DependencyManager.economy;
            int per = DataManager.getConfig().getInt("winMoneyPercent");
            double cMoney = eco.getBalance(DependencyManager.getCartels().getOwnerFromUUID(cartelUUID));
            int m = (int)((cMoney/100.0)*per);
            eco.depositPlayer(player.getName(), m);
            endTimer();
		}
	}
	
	private void endTimer(){
	    Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(DataManager.getPlugin(), new Runnable(){
	        public void run(){
	        	endAttack();
	        }
	    }, 60 * 20L);
	}
}

class PreviousStuff {
	public Player player;
    public Location getLocation;
    public ItemStack[] getInventory;
    public ItemStack[] getArmour;
    public double getHealth;
    public int getFood;
    public float getXP;
    public int getLevel;
    public GameMode getGameMode;
 
    PreviousStuff(Player p){
    	getLocation = p.getLocation();
		getInventory = p.getInventory().getContents();
		getArmour = p.getInventory().getArmorContents();
		getHealth = p.getHealth();
		getFood = p.getFoodLevel();
		getXP = p.getExp();
		getGameMode = p.getGameMode();
		getLevel = p.getLevel();
		player = p;
    }
    
    public void restore(){
		try{
			Location preLock = getLocation;
			int preFood = getFood;
			double preHealth = getHealth;
			float preXP = getXP;
			int preLvl = getLevel;
			ItemStack[] preInv = getInventory;
			ItemStack[] preAmour = getArmour;
			player.setFallDistance(0);
			player.teleport(preLock);
			player.setVelocity(new Vector(0, 0, 0));
			player.setFoodLevel(preFood);
			player.setFireTicks(0);
			player.setLevel(preLvl);
			player.setExp(preXP);
			player.setHealth(preHealth);
			//player.getInventory().clear();
			player.getInventory().setContents(preInv);
			player.getInventory().setArmorContents(preAmour);
			player.updateInventory();
			player.setGameMode(getGameMode);
		}catch(Exception e){}
    }
}
