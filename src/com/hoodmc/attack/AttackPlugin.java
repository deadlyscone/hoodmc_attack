package com.hoodmc.attack;

import com.hoodmc.attack.cmds.CommandRouter;
import com.hoodmc.attack.events.AttackerEvents;
import com.hoodmc.attack.events.GUIEvents;
import com.hoodmc.attack.events.GuardianEvents;
import com.hoodmc.attack.events.ProtocolEvents;
import com.hoodmc.attack.utils.DependencyManager;
import com.hoodmc.attack.utils.PluginLogger;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.hoodmc.attack.utils.DataManager;

public class AttackPlugin extends JavaPlugin{
	public static File saveYML;
	public static YamlConfiguration saveFile;
	public static boolean isHalting = false;
	
	//******************************************************************\
	//  On Enable
	//******************************************************************\
	@Override
	public void onEnable(){
		// Register Dependencies
		RegisterDependencies();
		// Check Files
		DataManager.CheckFiles();
		// Load Data
		DataManager.loadConfigData();
		DataManager.loadSavedData(this);
		// Register Events
		RegisterEvents();
		//DataManager.registerSaves();
		saveYML = new File(DataManager.getPlugin().getDataFolder(), "save.yml");
		saveFile = YamlConfiguration.loadConfiguration(saveYML);
	  	//Register Saves
	      if (!saveYML.exists()) {
	          try {
	          	saveYML.createNewFile();
	          } catch (IOException e) {
	              e.printStackTrace();
	          }
	      }
	}
	
	//******************************************************************\
	//  On Disable
	//******************************************************************\
	@Override
	public void onDisable(){
		// Save Data
		DataManager.saveData();
		// Wipe Data, Clear any open GUI's
		DataManager.ClearData(this);
	}
	
	//******************************************************************\
	//  Helper Functions
	//******************************************************************\
	private void RegisterEvents(){
		if(!isHalting){
			//  Listeners
			new ProtocolEvents(this);
			new GuardianEvents(this);
			new GUIEvents(this);
			new AttackerEvents(this);

			//  Commands
			//System.out.println("C: " + this.getCommand("cartel"));
			this.getCommand("cartel").setExecutor(new CommandRouter(this));
			//System.out.println("C: " + this.getCommand("turret"));
			this.getCommand("turret").setExecutor(new CommandRouter(this));
		}
	}

	private void RegisterDependencies(){
		DependencyManager dm = new DependencyManager(this);
		dm.setupEconomy();
		dm.setupProtocolLib();
		dm.checkCitizens();

	}
	public static void halt(Exception reason){
		isHalting = true;
		AttackPlugin plugin = (AttackPlugin) Bukkit.getServer().getPluginManager().getPlugin("HoodAttack");
		if(DataManager.inDebug){
			reason.printStackTrace();
		}else{
			//  Lets write it to an error log.
			reason.printStackTrace();	
		}
		PluginLogger.severe("Shutting Down! | Reason: " + reason.getMessage());
		Bukkit.getServer().getPluginManager().disablePlugin(plugin);
	}
}
