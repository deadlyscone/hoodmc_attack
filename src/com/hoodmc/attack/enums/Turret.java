package com.hoodmc.attack.enums;

import com.hoodmc.attack.AttackPlugin;

/**
 * Created by deadlyscone on 11/26/2015.
 */


    public abstract class Turret{

       public enum Type{
           Laser, Fireball, Landmine, Shade, Grenade_Launcher;

           public static Type asTurretType(String type, boolean haltOnError){
               for(Type t : Type.values()){
                   if(t.toString().replace(" ","_").equalsIgnoreCase(type)){
                       return t;
                   }
               }
               if(haltOnError){ AttackPlugin.halt(new Exception("UnknownTurretTypeException: " + type)); }
               return null;
           }

           public static String AsFString(String type){
               return type.replace("_"," ");
           }
           public static String AsFString(Type type){
               return AsFString(type.toString());
           }
        }

       public enum SpawnStatus{
           SPAWNED, NOT_SPAWNED, ENTITY_NEARBY, INSUFFICIENT_OBJECTS, ;
        }
    }
