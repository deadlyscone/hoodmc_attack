package com.hoodmc.attack.enums;

/**
 * Created by deadlyscone on 11/3/2015.
 */
public enum GUIViewType {
    TURRET_MENU, ATTACKER_MENU, MAIN_MENU;

    public static GUIViewType tryParse(String action){
        for(GUIViewType gvt :  GUIViewType.values()){
            if(gvt.toString().equalsIgnoreCase( (action.toUpperCase().replace(" ","_")) )){
                return gvt;
            }
        }
        return null;
    }

    /* Converts the ENUM type to a configString essential capitalizing each new word. */
    public static String asConfigString(GUIViewType type){
        String[] typeSplit = type.toString().replace("_"," ").toLowerCase().split(" ");
        String returnStr = "";
        for(int i = 0; i < typeSplit.length; i++){
            returnStr += Character.toUpperCase(typeSplit[i].charAt(0)) + typeSplit[i].substring(1);
            if((i+1) < typeSplit.length){
                returnStr += " ";
            }
        }
        return returnStr;
    }
}
