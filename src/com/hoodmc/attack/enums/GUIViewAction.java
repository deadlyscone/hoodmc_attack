package com.hoodmc.attack.enums;

/**
 * Created by deadlyscone on 11/8/2015.
 */
public enum GUIViewAction{
    BUY_TURRET, BUY_ATTACKER, ATTACK;

    public static GUIViewAction tryParse(String action){
        for(GUIViewAction gva :  GUIViewAction.values()){
            if(gva.toString().equalsIgnoreCase( (action.toUpperCase().replace(" ","_")) )){
                return gva;
            }
        }
        return null;
    }
}
