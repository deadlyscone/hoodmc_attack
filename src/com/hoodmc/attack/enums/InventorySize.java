package com.hoodmc.attack.enums;

public enum InventorySize {
	Size_1_Rows,Size_2_Rows,Size_3_Rows,Size_4_Rows,Size_5_Rows;

	public static Integer asInteger(InventorySize size){
		return Integer.valueOf(size.toString().split("\\D")[5])*9;
	}

	public static InventorySize toInventorySize(Integer sizeAmount){
		return InventorySize.valueOf("Size_" + String.valueOf(sizeAmount) + "_Rows");
	}
}
