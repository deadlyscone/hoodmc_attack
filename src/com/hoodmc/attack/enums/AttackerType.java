package com.hoodmc.attack.enums;

import com.hoodmc.attack.utils.AttackerManager;

public enum AttackerType {
Melee, Ranged, Kamikaze;

    public static AttackerType fromString(String type){

        for(AttackerType at : AttackerType.values()){
            if(at.toString().equalsIgnoreCase(type)){
                return at;
            }
        }
        return null;
    }
}
