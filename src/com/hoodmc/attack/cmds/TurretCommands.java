package com.hoodmc.attack.cmds;

import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.enums.Turret;
import com.hoodmc.attack.utils.HelperUtil;
import com.hoodmc.attack.utils.PluginLogger;
import com.hoodmc.attack.utils.TurretManager;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by deadlyscone on 11/21/2015.
 */
public class TurretCommands{

    private CommandSender sender;
    private String label;
    private String[] args;

    AttackPlugin plugin;
    public TurretCommands(AttackPlugin plugin){
        this.plugin = plugin;
    }

    public boolean createHook(HookInfo hookInfo){
        this.sender = hookInfo.getSender();
        this.label = hookInfo.getLabel();
        this.args = hookInfo.getArgs();
        return this.hookResult();
    }

    private boolean hookResult() {
        //if ((label.equalsIgnoreCase("attack"))) {
            if (sender instanceof Player) {
                Player player = ((Player) sender);

                if(args.length == 2 && args[0].equalsIgnoreCase("turret")){
                    HelperUtil.placeTurret(player.getLocation(), player, args[1], plugin);
                    return true;
                }
            } else {
                // CONSOLE COMMANDS HERE
            }
        //}
        return false;
    }
}
