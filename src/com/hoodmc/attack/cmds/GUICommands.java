package com.hoodmc.attack.cmds;

import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.enums.GUIViewType;
import com.hoodmc.attack.obj.TurretObject;
import com.hoodmc.attack.utils.DataManager;
import com.hoodmc.attack.utils.GUIManager;
import com.hoodmc.attack.utils.PluginLogger;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deadlyscone on 11/21/2015.
 */
public class GUICommands{

    private CommandSender sender;
    private String label;
    private String[] args;

    AttackPlugin plugin;
    public GUICommands(AttackPlugin plugin){
        this.plugin = plugin;
    }

    public boolean createHook(HookInfo hookInfo){
        this.sender = hookInfo.getSender();
        this.label = hookInfo.getLabel();
        this.args = hookInfo.getArgs();
        return this.hookResult();
    }

    private boolean hookResult() {
        if ((label.equalsIgnoreCase(DataManager.commandBase))) {
            if (sender instanceof Player) {
                Player player = ((Player) sender);
                System.out.println("s1");
                //if(args.length == 1 && args[0].equalsIgnoreCase("store")){
                    List<ItemStack> items = new ArrayList<ItemStack>();
                    for(TurretObject to : DataManager.TurretObjects){
                        if(to != null && to.getBlockTypeId() != null && to.getBlockTypeMeta() != null){
                            items.add(new ItemStack(to.getBlockTypeId(), 1, to.getBlockTypeMeta()));
                        }
                    }
                    GUIManager.findAndDisplayGUI(GUIViewType.MAIN_MENU, ((Player)sender));
                    return true;
                //}
            }
        }

        return false;
    }
}
