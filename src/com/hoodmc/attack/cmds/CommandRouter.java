package com.hoodmc.attack.cmds;

import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.utils.DataManager;
import com.hoodmc.attack.utils.PluginLogger;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deadlyscone on 11/21/2015.
 */
public class CommandRouter implements CommandExecutor{


    AttackPlugin plugin;
    private AttackCommands attackCommands;
    private GeneralCommands generalCommands;
    private GUICommands guiCommands;
    private TurretCommands turretCommands;
    private List<Boolean> Routes = new ArrayList<Boolean>();

    public CommandRouter(AttackPlugin plugin){
        this.plugin = plugin;
        attackCommands = new AttackCommands(plugin);
        generalCommands = new GeneralCommands(plugin);
        guiCommands = new GUICommands(plugin);
        turretCommands = new TurretCommands(plugin);
    }



    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        /*
              Routes to other command classes
         */
        final HookInfo hookInfo = new HookInfo(sender,cmd,label,args);
        boolean sendPluginHelp = true;

        Routes.add(attackCommands.createHook(hookInfo));
        Routes.add(generalCommands.createHook(hookInfo));
        Routes.add(turretCommands.createHook(hookInfo));
        Routes.add(guiCommands.createHook(hookInfo));

        for(boolean onCommandSuccess : Routes){ if(onCommandSuccess){ sendPluginHelp = false; break; } }
        if(sendPluginHelp){PluginLogger.showCommandHelp(sender, plugin);}
        Routes.clear();
        return true;
    }
}

class HookInfo {
    private CommandSender sender;
    private Command cmd;
    private String label;
    private String[] args;

    public HookInfo(CommandSender sender, Command cmd, String label, String[] args){
        this.args = args;
        this.cmd = cmd;
        this.label = label;
        this.sender = sender;
    }

    public String getLabel() {
        return label;
    }

    public String[] getArgs() {
        return args;
    }

    public Command getCmd() {
        return cmd;
    }

    public CommandSender getSender() {
        return sender;
    }
}
