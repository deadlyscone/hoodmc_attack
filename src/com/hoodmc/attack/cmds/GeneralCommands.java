package com.hoodmc.attack.cmds;

import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.utils.DataManager;
import com.hoodmc.attack.utils.PluginLogger;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


/**
 * Created by deadlyscone on 10/31/2015.
 */
public class GeneralCommands{

    private CommandSender sender;
    private String label;
    private String[] args;

    private final AttackPlugin plugin;

    public GeneralCommands(AttackPlugin plugin){ this.plugin = plugin; }

    public boolean createHook(HookInfo hookInfo){
        this.sender = hookInfo.getSender();
        this.label = hookInfo.getLabel();
        this.args = hookInfo.getArgs();
        return this.hookResult();
    }

    private boolean hookResult() {
        if ((label.equalsIgnoreCase("attack"))) {
            if (sender instanceof Player) {
                Player player = ((Player) sender);
                if(args[0].equalsIgnoreCase("reload")){
                    if (player.hasPermission("admin.reload")) {
                        DataManager.reloadData(plugin);
                        PluginLogger.sendPlayerError(player, "Reloaded.");//Just displays red, not really an error
                        return true;
                    }
                }
            } else {// Console

            }
        }
        return false;
    }
}
