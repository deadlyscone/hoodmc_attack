package com.hoodmc.attack.cmds;

import com.hoodmc.attack.AttackPlugin;
import com.hoodmc.attack.enums.AttackerType;
import com.hoodmc.attack.obj.AttackerObject;
import com.hoodmc.attack.utils.AttackerManager;
import com.hoodmc.attack.utils.DataManager;
import com.hoodmc.attack.utils.PluginLogger;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;


/**
 * Created by deadlyscone on 10/31/2015.
 */
public class AttackCommands{

    private CommandSender sender;
    private String label;
    private String[] args;
    AttackPlugin plugin;

    public AttackCommands(AttackPlugin plugin){
        this.plugin = plugin;
    }

    public boolean createHook(HookInfo hookInfo){
        this.sender = hookInfo.getSender();
        this.label = hookInfo.getLabel();
        this.args = hookInfo.getArgs();
        return this.hookResult();
    }

    private boolean hookResult() {
        if ((label.equalsIgnoreCase(DataManager.commandBase))) {
            if (sender instanceof Player) {
                Player player = ((Player) sender);
                if(args[0].equals("attacker")){
                	AttackerObject obj = null;
                	for(AttackerObject o : DataManager.AttackerObjects)
                		if(o != null && o.getType() != null && o.getType().equals(AttackerType.fromString(args[1])))
                			obj = o.clone();
                	if(obj != null){
                		AttackerManager.spawnAttackerFromObj(player, player.getLocation(), obj);
                	}else
                		player.sendMessage("§cAttacker type invalid!");
                }


                /*
                    Spawns the desired Attacker NPC
                 */

//                if(args.length == 2 && args[0].equalsIgnoreCase("attacker")){
//                    AttackerType desiredType = AttackerType.fromString(args[1]);
//                    if(desiredType != null){
//                        AttackerObject attackerObject = AttackerManager.getPlayerOwnedObject(desiredType, player);
//                        if(attackerObject != null){
//                            AttackerManager.spawnAttackerFromObj(player, player.getLocation(), attackerObject);
//                            return true;
//                        }
//                        PluginLogger.sendPlayerError(player, "Sorry, you currently do not own this type of attacker." +
//                                " type /" + label + " store to purchase one.");
//                    }else{
//                        PluginLogger.sendPlayerError(player, "Could not find Attacker called " + args[1] + ".");
//                    }
//                    return true; // Prevents the Command Help from displaying.
//                }



            } else {// Console

            }

        }
        return false;
    }
}
